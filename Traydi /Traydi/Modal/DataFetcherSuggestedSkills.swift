//
//  DataFetcherSuggestedSkills.swift
//  Traydi
//
//  Created by mac on 02/03/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import Foundation
/*
class DataFetcherSuggestedSkills: Codable {
    let code : String?
    let message : String?
    let suggested_skills : [SuggestedModal]?
    
    enum CodingKeys: String, CodingKey {
        
        case code = "code"
        case message = "message"
        case suggested_skills = "suggested_skills"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(String.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        suggested_skills = try values.decodeIfPresent([SuggestedModal].self, forKey: .suggested_skills)
    }

}

class SuggestedModal: Codable {
    /* id = 575;
               selectable = false;
               skills = "Chiller Work";
               "trade_id" = 1;
               type = skills;*/
    var id : String = ""
    var skills: String = ""
    var  selectable: Bool? = false
    var trade_id: Int?
    var   type   : String = ""
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case skills = "skills"
        case selectable = "selectable"
        case trade_id = "trade_id"
        case type = "type"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)!
        skills = try values.decodeIfPresent(String.self, forKey: .skills)!
         selectable = try values.decodeIfPresent(Bool.self, forKey: .selectable)
        type = try values.decodeIfPresent(String.self, forKey: .type)!
         trade_id = try values.decodeIfPresent(Int.self, forKey: .trade_id)
        
    }

}*/

//--
struct DataFetcherSuggestedSkills : Codable {
    let code : String?
    let message : String?
    var suggested_skills : [SuggestedModal]?

    enum CodingKeys: String, CodingKey {

        case code = "code"
        case message = "message"
        case suggested_skills = "suggested_skills"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(String.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        suggested_skills = try values.decodeIfPresent([SuggestedModal].self, forKey: .suggested_skills)
    }

}

struct SuggestedModal : Codable {
    let id : String?
    let trade_id : String?
    let skills : String?
    var selectable : String?
    let type : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case trade_id = "trade_id"
        case skills = "skills"
        case selectable = "selectable"
        case type = "type"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        trade_id = try values.decodeIfPresent(String.self, forKey: .trade_id)
        skills = try values.decodeIfPresent(String.self, forKey: .skills)
        selectable = try values.decodeIfPresent(String.self, forKey: .selectable)
        type = try values.decodeIfPresent(String.self, forKey: .type)
    }

}
