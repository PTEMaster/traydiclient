
import Foundation

class ImageModel : NSObject {
    var id : String?
    var user_id : String?
    var post_id : String?
    var image : String?
    var created_at : String?
    
    init(parameters:[String:Any]) {
        id = parameters["id"] as? String
        user_id = parameters["user_id"] as? String
        post_id = parameters["post_id"] as? String
        image = parameters["image"] as? String
        created_at = parameters["created_at"] as? String
    }
}
