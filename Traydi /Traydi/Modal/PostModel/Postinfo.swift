import Foundation
class Postinfo : NSObject {
    var id : String?
    var user_id : String?
    var user_type : String?
    var post_type : String?
    var create_post_type : String?
    var tittle : String?
    var descriptions : String?
    var total_like : String?
    var is_like : String?
    var total_comment : String?
    var created_at : String?
    var image : [ImageModel]?
    var profile_image : String?
    var comments : [Comment]?
    var full_name:String?
    var repost:String?
    var repost_by:String?
    var repost_full_name:String?
    var repost_image:String?
    var repost_user_type:String?
    
    
    
    init(parameters:[String:Any]) {
        
        full_name = parameters["full_name"] as? String
        id = parameters["id"] as? String
        user_id = parameters["user_id"] as? String
        user_type = parameters["user_type"] as? String
        post_type = parameters["post_type"] as? String
        create_post_type = parameters["create_post_type"] as? String
        tittle = parameters["tittle"] as? String
        descriptions = parameters["description"] as? String
        total_like = parameters["total_like"] as? String
        is_like = parameters["is_like"] as? String
        total_comment = parameters["total_comment"] as? String
        created_at = parameters["created_at"] as? String
        repost_user_type = parameters["repost_user_type"] as? String
        repost = parameters["repost"] as? String
        repost_by = parameters["repost_by"] as? String ?? ""
        repost_full_name = parameters["repost_full_name"] as? String ?? ""
        repost_image = parameters["repost_image"] as? String ?? ""
        profile_image = parameters["profile_image"] as? String
        
        let  arrimage = parameters["image"] as? [[String:Any]]
        var arrimagesmain = [ImageModel]()
        for items in arrimage! {
            let img = ImageModel(parameters: items)
            arrimagesmain.append(img)
        }
        image = arrimagesmain
        
        let arrCommen = parameters["comment"] as? [[String:Any]]
        var arrcomentmain = [Comment]()
        for item in arrCommen! {
            let comment = Comment(parameters: item)
            arrcomentmain.append(comment)
        }
        comments = arrcomentmain
    }
}


