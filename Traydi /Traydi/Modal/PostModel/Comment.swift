

import Foundation

class Comment : NSObject {
	var id : String?
	var user_id : String?
	var post_id : String?
	var comment : String?
    var full_name : String?
    var image : String?
    var total_like : String?
    var is_like  : String?
    var replies : [Replies]?
    init(parameters:[String:Any]){
        id = parameters["id"] as? String
        user_id = parameters["user_id"] as? String
        post_id = parameters["post_id"] as? String
        comment = parameters["comment"] as? String
        full_name = parameters["full_name"] as? String ?? ""
        image = parameters["image"] as? String ?? "" 
        total_like = parameters["total_like"]as? String
        is_like = parameters["is_like"] as? String
        let arrCommen = parameters["replies"] as? [[String:Any]]
        var arrcomentmain = [Replies]()
        if arrCommen != nil {
            if  arrCommen?.count ?? 0 > 0{
                for item in arrCommen!{
                    let comment = Replies(parameters: item)
                    arrcomentmain.append(comment)
                }
            }
        }
        replies = arrcomentmain
    }

}

class Replies : NSObject {
    var id : String?
    var comment_id : String?
    var full_name : String?
    var image : String = ""
    var post_id : String?
    var reply : String?
    var user_id : String?
    
    init(parameters:[String:Any]){
        id = parameters["id"] as? String
        comment_id = parameters["comment_id"] as? String
        full_name = parameters["full_name"] as? String
        image = parameters["image"] as? String ?? "" 
        post_id = parameters["post_id"] as? String
        reply = parameters["reply"] as? String
        user_id = parameters["user_id"] as? String
    }
}

