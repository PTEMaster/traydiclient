

import Foundation

class LikeModel : NSObject {
    var code : String?
    var message : String?
    var likes : String?
    var liked : Int?
    
    init(parameters:[String:Any]) {
        code = parameters["code"] as? String
        message = parameters["message"] as? String
        likes = parameters["likes"] as? String
        liked = parameters["liked"] as? Int
    }
}
