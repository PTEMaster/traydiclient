


import Foundation

class CommentModel : NSObject {
	var code : String?
	var message : String?
	var comment : Int?
    init(parameters:[String:Any]) {
		 code = parameters["code"] as? String
		 message = parameters["message"] as? String
		 comment = parameters["comment"] as? Int
	}
}
