

import Foundation
struct DataFetcherWorkTypeModal : Codable {
	let code : String?
	let message : String?
    var type_of_trades : [Type_of_trades]?

	enum CodingKeys: String, CodingKey {

		case code = "code"
		case message = "message"
		case type_of_trades = "type_of_trades"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		code = try values.decodeIfPresent(String.self, forKey: .code)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		type_of_trades = try values.decodeIfPresent([Type_of_trades].self, forKey: .type_of_trades)
	}
}
