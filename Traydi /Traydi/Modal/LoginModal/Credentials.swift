
import Foundation
struct Credentials : Codable {
	let id : String?
	let tittle : String?
	let provider : String?
	let number : String?
	let expiry_date : String?
	let user_type : String?
	let user_id : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case tittle = "tittle"
		case provider = "provider"
		case number = "number"
		case expiry_date = "expiry_date"
		case user_type = "user_type"
		case user_id = "user_id"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		tittle = try values.decodeIfPresent(String.self, forKey: .tittle)
		provider = try values.decodeIfPresent(String.self, forKey: .provider)
		number = try values.decodeIfPresent(String.self, forKey: .number)
		expiry_date = try values.decodeIfPresent(String.self, forKey: .expiry_date)
		user_type = try values.decodeIfPresent(String.self, forKey: .user_type)
		user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
	}

}
