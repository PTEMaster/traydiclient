
import Foundation
struct LoginModal : Codable {
	let code : String?
	let message : String?
    var userinfo : UserinfoModal?

	enum CodingKeys: String, CodingKey {

		case code = "code"
		case message = "message"
		case userinfo = "tradersinfo"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		code = try values.decodeIfPresent(String.self, forKey: .code)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		userinfo = try values.decodeIfPresent(UserinfoModal.self, forKey: .userinfo)
	}
}
