
import Foundation
struct Pages : Codable {
	let id : String?
	let user_id : String?
	let page_tittle : String?
	let image : String?
	let created_at : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case user_id = "user_id"
		case page_tittle = "page_tittle"
		case image = "image"
		case created_at = "created_at"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
		page_tittle = try values.decodeIfPresent(String.self, forKey: .page_tittle)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
	}

}
