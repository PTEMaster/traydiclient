
import Foundation
struct UserinfoModal : Codable {
	let id : String?
	let user_type : String?
	let full_name : String?
	let email : String?
	let address : String?
	let city : String?
	let country_code : String?
	let mobile_number : String?
	let mobile_otp : String?
	let password : String?
	let image : String?
	let verification_id : String?
	let created_date : String?
	let device_token : String?
	let device_type : String?
	var type_of_trade :  [Type_of_trade]?
	let work_type : String?
	let skill_level : String?
	let year_of_experience : String?
	let apprentice : String?
	let business_name : String?
	let aBN_CAN : String?
	let website : String?
	let business_experience : String?
	let skill_needed : String?
	let suggested_skills :  [Suggested_skills]?
    var  trade_needed   : String?
	let work_radius : String?
	let latitude : String?
	let longitude : String?
	let postalcode : String?
	let distance : String?
	let step_status : String?
	let profile_image_status : String?
	let work_type_status : String?
	let skill_level_status : String?
	let business_details_status : String?
	let skill_needed_status : String?
	let work_radius_status : String?
	let credentials_status : String?
	let followers : String?
	let following : String?
	let active_project : String?
	let completed_project : String?
	let notify_on_connection : String?
	let notify_on_message : String?
	let notify_on_application : String?
	let notify_on_review : String?
	let notify_on_comment : String?
	let notify_on_hour_adjust : String?
	let notify_on_profile : String?
	let notify_on_tradee_view : String?
	let notify_on_likes : String?
	let notify_on_connection_accepted : String?
	let notifiy_on_clock : String?
	let notifiy_on_job_complete : String?
	let privacy_status : String?
	let pages : [Pages]?
	let credentials : [Credentials]?
	let step_completed_percentage : String?

	enum CodingKeys: String, CodingKey {
        case trade_needed  = "trade_needed"
		case id = "id"
		case user_type = "user_type"
		case full_name = "full_name"
		case email = "email"
		case address = "address"
		case city = "city"
		case country_code = "country_code"
		case mobile_number = "mobile_number"
		case mobile_otp = "mobile_otp"
		case password = "password"
		case image = "image"
		case verification_id = "verification_id"
		case created_date = "created_date"
		case device_token = "device_token"
		case device_type = "device_type"
		case type_of_trade = "type_of_trade"
		case work_type = "work_type"
		case skill_level = "skill_level"
		case year_of_experience = "year_of_experience"
		case apprentice = "apprentice"
		case business_name = "business_name"
		case aBN_CAN = "ABN_CAN"
		case website = "website"
		case business_experience = "business_experience"
		case skill_needed = "skill_needed"
		case suggested_skills = "suggested_skills"
		case work_radius = "work_radius"
		case latitude = "latitude"
		case longitude = "longitude"
		case postalcode = "postalcode"
		case distance = "distance"
		case step_status = "step_status"
		case profile_image_status = "profile_image_status"
		case work_type_status = "work_type_status"
		case skill_level_status = "skill_level_status"
		case business_details_status = "business_details_status"
		case skill_needed_status = "skill_needed_status"
		case work_radius_status = "work_radius_status"
		case credentials_status = "credentials_status"
		case followers = "followers"
		case following = "following"
		case active_project = "active_project"
		case completed_project = "completed_project"
		case notify_on_connection = "notify_on_connection"
		case notify_on_message = "notify_on_message"
		case notify_on_application = "notify_on_application"
		case notify_on_review = "notify_on_review"
		case notify_on_comment = "notify_on_comment"
		case notify_on_hour_adjust = "notify_on_hour_adjust"
		case notify_on_profile = "notify_on_profile"
		case notify_on_tradee_view = "notify_on_tradee_view"
		case notify_on_likes = "notify_on_likes"
		case notify_on_connection_accepted = "notify_on_connection_accepted"
		case notifiy_on_clock = "notifiy_on_clock"
		case notifiy_on_job_complete = "notifiy_on_job_complete"
		case privacy_status = "privacy_status"
		case pages = "pages"
		case credentials = "credentials"
		case step_completed_percentage = "step_completed_percentage"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
        trade_needed = try values.decodeIfPresent(String.self, forKey: .trade_needed)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		user_type = try values.decodeIfPresent(String.self, forKey: .user_type)
		full_name = try values.decodeIfPresent(String.self, forKey: .full_name)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		address = try values.decodeIfPresent(String.self, forKey: .address)
		city = try values.decodeIfPresent(String.self, forKey: .city)
		country_code = try values.decodeIfPresent(String.self, forKey: .country_code)
		mobile_number = try values.decodeIfPresent(String.self, forKey: .mobile_number)
		mobile_otp = try values.decodeIfPresent(String.self, forKey: .mobile_otp)
		password = try values.decodeIfPresent(String.self, forKey: .password)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		verification_id = try values.decodeIfPresent(String.self, forKey: .verification_id)
		created_date = try values.decodeIfPresent(String.self, forKey: .created_date)
		device_token = try values.decodeIfPresent(String.self, forKey: .device_token)
		device_type = try values.decodeIfPresent(String.self, forKey: .device_type)
		type_of_trade = try values.decodeIfPresent([Type_of_trade].self, forKey: .type_of_trade)
		work_type = try values.decodeIfPresent(String.self, forKey: .work_type)
		skill_level = try values.decodeIfPresent(String.self, forKey: .skill_level)
		year_of_experience = try values.decodeIfPresent(String.self, forKey: .year_of_experience)
		apprentice = try values.decodeIfPresent(String.self, forKey: .apprentice)
		business_name = try values.decodeIfPresent(String.self, forKey: .business_name)
		aBN_CAN = try values.decodeIfPresent(String.self, forKey: .aBN_CAN)
		website = try values.decodeIfPresent(String.self, forKey: .website)
		business_experience = try values.decodeIfPresent(String.self, forKey: .business_experience)
		skill_needed = try values.decodeIfPresent(String.self, forKey: .skill_needed)
		suggested_skills = try values.decodeIfPresent([Suggested_skills].self, forKey: .suggested_skills)
		work_radius = try values.decodeIfPresent(String.self, forKey: .work_radius)
		latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
		longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
		postalcode = try values.decodeIfPresent(String.self, forKey: .postalcode)
		distance = try values.decodeIfPresent(String.self, forKey: .distance)
		step_status = try values.decodeIfPresent(String.self, forKey: .step_status)
		profile_image_status = try values.decodeIfPresent(String.self, forKey: .profile_image_status)
		work_type_status = try values.decodeIfPresent(String.self, forKey: .work_type_status)
		skill_level_status = try values.decodeIfPresent(String.self, forKey: .skill_level_status)
		business_details_status = try values.decodeIfPresent(String.self, forKey: .business_details_status)
		skill_needed_status = try values.decodeIfPresent(String.self, forKey: .skill_needed_status)
		work_radius_status = try values.decodeIfPresent(String.self, forKey: .work_radius_status)
		credentials_status = try values.decodeIfPresent(String.self, forKey: .credentials_status)
		followers = try values.decodeIfPresent(String.self, forKey: .followers)
		following = try values.decodeIfPresent(String.self, forKey: .following)
		active_project = try values.decodeIfPresent(String.self, forKey: .active_project)
		completed_project = try values.decodeIfPresent(String.self, forKey: .completed_project)
		notify_on_connection = try values.decodeIfPresent(String.self, forKey: .notify_on_connection)
		notify_on_message = try values.decodeIfPresent(String.self, forKey: .notify_on_message)
		notify_on_application = try values.decodeIfPresent(String.self, forKey: .notify_on_application)
		notify_on_review = try values.decodeIfPresent(String.self, forKey: .notify_on_review)
		notify_on_comment = try values.decodeIfPresent(String.self, forKey: .notify_on_comment)
		notify_on_hour_adjust = try values.decodeIfPresent(String.self, forKey: .notify_on_hour_adjust)
		notify_on_profile = try values.decodeIfPresent(String.self, forKey: .notify_on_profile)
		notify_on_tradee_view = try values.decodeIfPresent(String.self, forKey: .notify_on_tradee_view)
		notify_on_likes = try values.decodeIfPresent(String.self, forKey: .notify_on_likes)
		notify_on_connection_accepted = try values.decodeIfPresent(String.self, forKey: .notify_on_connection_accepted)
		notifiy_on_clock = try values.decodeIfPresent(String.self, forKey: .notifiy_on_clock)
		notifiy_on_job_complete = try values.decodeIfPresent(String.self, forKey: .notifiy_on_job_complete)
		privacy_status = try values.decodeIfPresent(String.self, forKey: .privacy_status)
		pages = try values.decodeIfPresent([Pages].self, forKey: .pages)
		credentials = try values.decodeIfPresent([Credentials].self, forKey: .credentials)
		step_completed_percentage = try values.decodeIfPresent(String.self, forKey: .step_completed_percentage)
	}
    
   struct Type_of_trade : Codable {
        let id : String?
        let selectable : String?
        let type_of_trade : String?

        enum CodingKeys: String, CodingKey {

            case id = "id"
            case selectable = "selectable"
            case type_of_trade = "type_of_trade"
        }

        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(String.self, forKey: .id)
            selectable = try values.decodeIfPresent(String.self, forKey: .selectable)
            type_of_trade = try values.decodeIfPresent(String.self, forKey: .type_of_trade)
        }

    }
    
    struct Suggested_skills : Codable {
        let id : String?
        let selectable : String?
        let skills : String?
        let  trade_id : String?
        let  type : String?
        
        enum CodingKeys: String, CodingKey {
            case id = "id"
            case selectable = "selectable"
            case skills = "skills"
            case  trade_id  = "trade_id"
            case  type = "type"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(String.self, forKey: .id)
            selectable = try values.decodeIfPresent(String.self, forKey: .selectable)
            skills = try values.decodeIfPresent(String.self, forKey: .skills)
             trade_id = try values.decodeIfPresent(String.self, forKey: .trade_id)
             type = try values.decodeIfPresent(String.self, forKey: .type)
        }
        
    }
//suggested_skills/

}
