// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let workType = try? newJSONDecoder().decode(WorkType.self, from: jsonData)

import Foundation

// MARK: - WorkType
struct WorkType: Codable {
    let code, message: String?
    let tradersinfo: Tradersinfo?
}

// MARK: - Tradersinfo
struct Tradersinfo: Codable {
    let id, userType, fullName, email: String?
    let address, city, countryCode, mobileNumber: String?
    let mobileOtp, password, image, verificationID: String?
    let createdDate, deviceToken, deviceType: String?
    let typeOfTrade: [TypeOfTrade]?
    let tradeNeeded, workType, skillLevel, yearOfExperience: String?
    let apprentice, businessName, abnCan, website: String?
    let businessExperience, skillNeeded: String?
    let suggestedSkills: [JSONAny]?
    let workRadius, latitude, longitude, postalcode: String?
    let distance, stepStatus, profileImageStatus, workTypeStatus: String?
    let skillLevelStatus, businessDetailsStatus, skillNeededStatus, workRadiusStatus: String?
    let credentialsStatus, followers, following, activeProject: String?
    let completedProject, notifyOnConnection, notifyOnMessage, notifyOnApplication: String?
    let notifyOnReview, notifyOnComment, notifyOnHourAdjust, notifyOnProfile: String?
    let notifyOnTradeeView, notifyOnLikes, notifyOnConnectionAccepted, notifiyOnClock: String?
    let notifiyOnJobComplete, privacyStatus, type: String?
    let credentials, pages: [JSONAny]?

    enum CodingKeys: String, CodingKey {
        case id
        case userType = "user_type"
        case fullName = "full_name"
        case email, address, city
        case countryCode = "country_code"
        case mobileNumber = "mobile_number"
        case mobileOtp = "mobile_otp"
        case password, image
        case verificationID = "verification_id"
        case createdDate = "created_date"
        case deviceToken = "device_token"
        case deviceType = "device_type"
        case typeOfTrade = "type_of_trade"
        case tradeNeeded = "trade_needed"
        case workType = "work_type"
        case skillLevel = "skill_level"
        case yearOfExperience = "year_of_experience"
        case apprentice
        case businessName = "business_name"
        case abnCan = "ABN_CAN"
        case website
        case businessExperience = "business_experience"
        case skillNeeded = "skill_needed"
        case suggestedSkills = "suggested_skills"
        case workRadius = "work_radius"
        case latitude, longitude, postalcode, distance
        case stepStatus = "step_status"
        case profileImageStatus = "profile_image_status"
        case workTypeStatus = "work_type_status"
        case skillLevelStatus = "skill_level_status"
        case businessDetailsStatus = "business_details_status"
        case skillNeededStatus = "skill_needed_status"
        case workRadiusStatus = "work_radius_status"
        case credentialsStatus = "credentials_status"
        case followers, following
        case activeProject = "active_project"
        case completedProject = "completed_project"
        case notifyOnConnection = "notify_on_connection"
        case notifyOnMessage = "notify_on_message"
        case notifyOnApplication = "notify_on_application"
        case notifyOnReview = "notify_on_review"
        case notifyOnComment = "notify_on_comment"
        case notifyOnHourAdjust = "notify_on_hour_adjust"
        case notifyOnProfile = "notify_on_profile"
        case notifyOnTradeeView = "notify_on_tradee_view"
        case notifyOnLikes = "notify_on_likes"
        case notifyOnConnectionAccepted = "notify_on_connection_accepted"
        case notifiyOnClock = "notifiy_on_clock"
        case notifiyOnJobComplete = "notifiy_on_job_complete"
        case privacyStatus = "privacy_status"
        case type, credentials, pages
    }
}

// MARK: - TypeOfTrade
struct TypeOfTrade: Codable {
    let id, selectable, typeOfTrade: String?

    enum CodingKeys: String, CodingKey {
        case id, selectable
        case typeOfTrade = "type_of_trade"
    }
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

class JSONCodingKey: CodingKey {
    let key: String

    required init?(intValue: Int) {
        return nil
    }

    required init?(stringValue: String) {
        key = stringValue
    }

    var intValue: Int? {
        return nil
    }

    var stringValue: String {
        return key
    }
}

class JSONAny: Codable {

    let value: Any

    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }

    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }

    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if let value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if let value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }

    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }

    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }

    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }

    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}
