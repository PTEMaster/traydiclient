//
//  DataFetcherModal.swift
//  Traydi
//
//  Created by mac on 29/02/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import Foundation


class DataFetcherModal: Codable {
    let code : String?
    let message : String?
    let userinfo : DataFetcherUserInfo?

    enum CodingKeys: String, CodingKey {

        case code = "code"
        case message = "message"
        case userinfo = "tradersinfo"
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(String.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        userinfo = try values.decodeIfPresent(DataFetcherUserInfo.self, forKey: .userinfo)
    }
}

class DataFetcherUserInfo: Codable {
    let id : String?
    let trade_needed : String?
    let type_of_trade :  String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
         case trade_needed  = "trade_needed"
        case type_of_trade = "type_of_trade"
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        type_of_trade = try values.decodeIfPresent(String.self, forKey: .type_of_trade)
         trade_needed = try values.decodeIfPresent(String.self, forKey: .trade_needed)
    }
}
