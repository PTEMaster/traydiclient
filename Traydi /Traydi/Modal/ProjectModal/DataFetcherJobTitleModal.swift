//
//  DataFetcherJobTitle.swift
//  Traydi
//
//  Created by mac on 23/04/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import Foundation

class DataFetcherJobTitleModal: NSObject {
    var id:String?
    var name:String?
     var type_of_trade:String?
    
    init(parameters:[String:Any]) {
        id = parameters["id"] as? String
        name = parameters["name"] as? String
        type_of_trade = parameters["type_of_trade"] as? String
    }
}

class DataFetcherRequiredSkillsModal: NSObject {
    var id:String?
    var skills:String?
    
    init(parameters:[String:Any]) {
        id = parameters["id"] as? String ?? ""
        skills = parameters["skills"] as? String
    }
}
