//
//  DataFetcherMyCrewModal.swift
//  Traydi
//
//  Created by mac on 19/05/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import Foundation

class DataFetcherMyCrewModal: NSObject {
    var status:Bool?
    var message:String?
    var sectionCrew: [SectionModal]?
    
    init(parameters:[String:Any]) {
      let code = parameters["code"] as? String
        if code == ApiResponseStatus.kAPI_Result_Success {
            status = true
        } else {
            status = false
        }
        message = parameters["message"] as? String
       let projects = parameters["projectlist"] as? [[String:Any]]
        var sectionS = [SectionModal]()
        for item in projects! {
            let dict = SectionModal(parameters: item, projects: projects!)
            sectionS.append(dict)
        }
        sectionCrew = sectionS
    }
}

class SectionModal: NSObject {
    var user_id:String?
    var project_name:String?
    var project_start_time:String?
    var project_end_time:String?
    var projectList:[ProjectListMyCrewModal]?
    var isExpended:Bool = false
    
    init(parameters:[String:Any], projects:[[String:Any]]) {
        user_id = parameters["user_id"] as? String
        project_name = parameters["project_name"] as? String
        project_start_time = parameters["project_start_time"] as? String
        project_end_time = parameters["project_end_time"] as? String
        let project = ProjectListMyCrewModal(parameters: parameters)
        projectList? = [project]
    }
}

class ProjectListMyCrewModal: NSObject {
    var id:String?
    var user_id:String?
    var project_name:String?
    var project_type:String?
    var project_start_time:String?
    var project_end_time:String?
    var job_end_time:String?
    var job_start_time:String?
    var required_resources:String?
    var address:String?
    var latitude:String?
    var longitude:String?
    var step1:String?
    var step2:String?
    var step3:String?
    var step4:String?
    var project_status:String?
    var progress_status:String?
    var status_manage:String?
    var resourcesData:[ResourcesMyCrewModal]?
    
    init(parameters:[String:Any]) {
        id = parameters["id"] as? String
        user_id = parameters["user_id"] as? String
        project_type = parameters["project_type"] as? String
        project_name = parameters["project_name"] as? String
        project_start_time = parameters["project_start_time"] as? String
        project_end_time = parameters["project_end_time"] as? String
        job_end_time = parameters["job_end_time"] as? String
        job_start_time = parameters["job_start_time"] as? String
        required_resources = parameters["required_resources"] as? String
        address = parameters["address"] as? String
        latitude = parameters["latitude"] as? String
        longitude = parameters["longitude"] as? String
        step1 = parameters["step1"] as? String
        step2 = parameters["step2"] as? String
        step3 = parameters["step3"] as? String
        step4 = parameters["step4"] as? String
        project_status = parameters["project_status"] as? String
        progress_status = parameters["progress_status"] as? String
        status_manage = parameters["status_manage"] as? String
        guard let resourcesList = parameters["resources"] as? [[String:Any]] else { return  }
        var resourcesModal = [ResourcesMyCrewModal]()
        for item in resourcesList {
            let resource = ResourcesMyCrewModal(parameters: item)
            resourcesModal.append(resource)
        }
        resourcesData = resourcesModal
    }
    
}

class ResourcesMyCrewModal: NSObject {
    var address:String?
    var latitude:String?
    var longitude:String?
    var job_type:String?
    var job_post:String?
    var working_skills:String?
    var job_description:String?
    var resource:String?
    var skills:String?
    var define_rate:String?
    var payment:String?
    var credential:String?
  
    init(parameters:[String:Any]) {
        address = parameters["address"] as? String
        latitude = parameters["latitude"] as? String
        longitude = parameters["longitude"] as? String
        job_type = parameters["job_type"] as? String
        job_post = parameters["job_post"] as? String
        working_skills = parameters["working_skills"] as? String
        job_description = parameters["job_description"] as? String
        resource = parameters["resource"] as? String
        skills = parameters["skills"] as? String
        define_rate = parameters["define_rate"] as? String
        payment = parameters["payment"] as? String
        credential = parameters["credential"] as? String
    }
    
}


 
