//
//  DataFetcherCredentialsModal.swift
//  Traydi
//
//  Created by mac on 02/03/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import Foundation

class DataFetcherCredentialsModal: Codable {
    let code : String?
    let message : String?
    let credentials : [CredentialModal]?
    
    enum CodingKeys: String, CodingKey {
        
        case code = "code"
        case message = "message"
        case credentials = "credentials"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(String.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        credentials = try values.decodeIfPresent([CredentialModal].self, forKey: .credentials)
    }
    
}

class CredentialModal: Codable {
    
    let id : String?
    let title: String?
    let provider: String?
    let number: String?
    let expiry_date: String?
    let user_type: String?
    let user_id: String?
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case title = "tittle"
        case provider = "provider"
        case number = "number"
        case expiry_date = "expiry_date"
        case user_type = "user_type"
        case user_id = "user_id"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        provider = try values.decodeIfPresent(String.self, forKey: .provider)
        number = try values.decodeIfPresent(String.self, forKey: .number)
        expiry_date = try values.decodeIfPresent(String.self, forKey: .expiry_date)
        user_type = try values.decodeIfPresent(String.self, forKey: .user_type)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
    }
    
}

