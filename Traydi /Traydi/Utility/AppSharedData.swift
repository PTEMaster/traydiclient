//
//  AppSharedData.swift
//  Traydi
//
//  Created by mac on 10/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import Foundation
class AppSharedData {
    static let shared = AppSharedData()
    var isSeeker:Bool{
        get{
            var _seeker = false
            if let seeker = UserDefaults.standard.value(forKey: "isSeeker") as? Bool{
                _seeker = seeker
            }else{
                _seeker = false
                
                // FIXME: HIMANSHU PAL 28/8/20
            }
            return _seeker
        }
    }
    var isUserGuideSkipped:Bool{
        get{
            var _isUserGuideSkipped = false
            if let gs = UserDefaults.standard.value(forKey: "isUserGuideSkipped") as? Bool{
                _isUserGuideSkipped = gs
            }else{_isUserGuideSkipped = false}
            
            return _isUserGuideSkipped
            
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "isUserGuideSkipped")
            UserDefaults.standard.synchronize()
           // self.isUserGuideSkipped = newValue
        }
    }
    private init(){
    
    }
    
    func timeInterval(timeAgo:String) -> String
    {
        let df = DateFormatter()
         let dateFormat = "dd-MM-yyyy HH:mm:ss"

        df.dateFormat = dateFormat
        let dateWithTime = df.date(from: timeAgo)

        let interval = Calendar.current.dateComponents([.calendar,.year, .month, .day, .hour, .minute, .second], from: dateWithTime!, to: Date())

        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + "year ago" : "\(year)" + " " + "years ago"
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + "month ago" : "\(month)" + " " + "months ago"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + "day ago" : "\(day)" + " " + "days ago"
        }else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour)" + " " + "hour ago" : "\(hour)" + " " + "hours ago"
        }else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "\(minute)" + " " + "minute ago" : "\(minute)" + " " + "minutes ago"
        }else if let second = interval.second, second > 0 {
            return second == 1 ? "\(second)" + " " + "second ago" : "\(second)" + " " + "seconds ago"
        } else {
            return "a moment ago"

        }
    }
    
}

