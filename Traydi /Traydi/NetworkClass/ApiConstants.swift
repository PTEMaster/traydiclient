//
//  APIComponet.swift
//  Traydi  https://traydi.com/traders/post_list       https://ctinfotech.com/CT25/traydi/


import UIKit
//let kAPI_BaseURL = "https://ctinfotech.com/CT25/traydi/"  // old
let kAPI_BaseURL = "https://traydi.com/" //new
let kTradees = "tradees/"
let kTraders = "traders/"
let AppName = "Traydi"

let authkey = "ODQ3ODk0NzY5";
let PhoneNumberLength = 12
let MinPhoneNumberLength = 4
let googleAPIKey = "AIzaSyDcEiUG78EnKaT2YTYEmJndBHjRW5mnb10"//

struct ImagesUrl {
    //old
    /*
     static let storyImage = "http://ctinfotech.com/CT25/traydi/assets/stories/";
     static let baseProfileUri = "https://ctinfotech.com/CT25/traydi/assets/userfile/profile/";
     static let basePostUri = "http://ctinfotech.com/CT25/traydi/assets/post/";
     static let documentUrl = "https://ctinfotech.com/CT25/traydi/assets/document/"
     */
    //new
    
    static let storyImage = "https://traydi.com/assets/stories/";
    static let baseProfileUri = "https://traydi.com/assets/userfile/profile/";
    static let basePostUri = "https://traydi.com/assets/post/";
    static let documentUrl = "https://traydi.com/assets/document/"
}

let appColor = UIColor(named: "colorBaseApp")
class APPConstants: NSObject {
    static let emailAcceptableCharacter = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_.-@"
    static let AddressAcceptableCharacter = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890,/- "
    static let NameAcceptableCharacter = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "
    static let ZipCodeAcceptableCharacter = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
    static let StateCharacter = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 ."
    static let phoneNoAcceptableCharacter = "1234567890"
}

struct appDelegate {
    static let shared = UIApplication.shared.delegate as! AppDelegate
}



//** Other Api Constants
let kDevicePlatform      = "iOS"
struct ApiAction {
    struct TradersAndTradees {
        static let ksignup = "signup"
        static let kwork_type = "work_type"
        static let kskill_level = "skill_level"
        static let kbusiness_details = "business_details"
        static let kwork_radius = "work_radius"
        static let kskill_needed = "skill_needed"
        static let kcredentials = "credentials"
        static let kcredentials_list = "credentials_list"
        static let klogin = "login"
        static let klogout = "logout"
        static let ktype_of_trade = "type_of_trade"
        static let ktrade_needed = "trade_needed"
        static let kprofile_picture = "profile_picture"
        static let kyear_of_experience = "year_of_experience"
        static let kadd_post = "add_post"
        static let kedit_post = "edit_post"
        static let kadd_story = "add_story"
        static let kSuggested_skills = "suggested_skills"
        static let kTags = "tags"
        static let kLikePost = "like_post"
        static let kPostList = "post_list"
        static let kpost_list_by_user = "post_list_by_user"
        static let kCommentPost = "comment_post"
        static let kadd_page = "add_page"
        static let kPages_list = "pages_list"
        static let kmy_story = "my_story"
        static let kpages_details = "pages_details"
        static let kjob_tittle = "job_tittle"
        static let krequired_skills = "required_skills"
        static let kadd_project = "add_project"
        static let kedit_project = "edit_project"
        static let kadd_lead = "add_lead"
        static let kedit_lead = "edit_lead"
        static let kdelete_lead = "delete_lead"
        static let kstep1 = "step1"
        static let kstep2 = "step2"
        static let kstep3 = "step3"
        static let kstep4 = "step4"
        static let kcertification = "certification"
        static let kcontact_us = "contact_us"
        static let kdocument_type = "document_type"
        static let kget_notification = "get_notification"
        static let kprivacy_on_off = "privacy_on_off"
        static let knotification_on_off = "notification_on_off"
        static let kproject_list = "project_list"
        static let klead_list = "lead_list"
        static let kqualification_document = "qualification_document"
        static let kedit_profile = "edit_profile"
        static let kqualification_document_list = "qualification_document_list"
        static let kcomment_post = "comment_post"
        static let kjob_list = "job_list"
        static let kbooking_list = "booking_list"
        static let kactive_booking = "active_booking"
        static let kpunch_in = "punch_in"
        static let kpunch_out = "punch_out"
        static let krequest_list = "request_list"
        static let krequest_send_list = "request_send_list"
        static let kdelete_request = "delete_request"
        static let kaccept_request = "accept_request"
        static let kfollow = "follow"
        static let kmy_crew = "my_crew"
        static let  kpostalcode = "postalcode"
        // FIXME: HIMANSHU PAL 18/8/20
        static let  ksearch_users = "search_users"
        static let  kmessage_list = "message_list"
        static let  knotification_list = "notification_list"
        static let kcomment_reply = "comment_reply"
        static let klike_comment = "like_comment"
        static let comment_story = "comment_story"
        static let kdelete_post = "delete_post"
        static let kreport = "report"
        static let krepost = "repost"
        static let kadd_reaction = "add_reaction"
        static let ksend_image = "send_image"
        static let kdelete_project = "delete_project"
        static let kleadlist = "leadlist"
        static let kapplyjob = "apply_job"
        static let kview_applicant = "view_applicant"
         static let khire = "hire"
        static let kprofile_detail = "profile_detail"
        
        
       
        
        
        
    }
}

struct ApiResponseStatus {
    static let kAPI_Result_Success = "1"
    static let kAPI_Result_Failure = "0"
    static let kMessage = "message"
    static let kcode = "code"
}



struct UseCaseMessage {
    struct validation {
        struct empty {
            static let kemail = "Please enter your email"
            static let kpassword = "Please enter you password"
            static let kemailValidation = "Please enter valid email"
            static let kmanually = "Please first time manually login"
            static let kAttendance = "Please select Attendance"
            static let kPhoneNumberTextField  = "Please enter phone number"
            static let kPhoneNumberValidation = "Please enter valid phone number"
            static let kCountryCodeTextField  = "Please select country code"
            static let kFullNameTextField = "Please enter full name"
            static let kcityTextField = "Please enter city"
            static let kConfirmPasswordTextField = "Please enter confirm password"
            static let kPasswordNotMatch = "Confirm password not match"
            static let kProfileImage = "Please select profile"
            static let kdocImage = "Please upload verification doucment"
            static let kWorkTypeOfTtrade = "Please select type of trade"
            static let kWorkTypes = "Please select work type"
            static let kBusinessName = "Please enter business name"
            static let kWebSide = "Please enter webside url"
            static let kbusiness_years = "Please select of Years"
            static let kCANNumber = "Please enter ABN/CAN number"
            static let kTitle = "Please enter title"
            static let kProvider = "Please enter provider"
            static let kDate = "Please enter expiry date"
            static let knumber = "Please enter number"
            static let kPageImage = "Please select page image"
            static let kProject = "Please enter project name"
            static let kProjectStartDate = "Please select project start date"
            static let kProkectDate = "Please select project end date"
            static let kJobStartTime = "Please enter job start time"
            static let kJobEndTime = "Please enter job and end time"
            
            
            static let kResoucrce_Required = "Please select Resoucrce Required"
            static let kJobTitle = "Please select job title"
            static let kWorkerSkills = "Please select worker skills"
            static let kdescriptions = "Please enter descriptions"
            static let kdistance = "Please select distance"
            static let kPostcode = "Please enter Subhurb or postcode"
            static let kWorkerSkillsContinue    = " Please select two more skills required to continue"
            
            // FIXME: HIMANSHU PAL
            static let kSkillLevel = "Please select your skill level"
            static let kApprentice = "Please select Apprentice"
            static let kExp_years = "Please select year of experience"
            static let kyears = "Please select years"
            static let kCustomerName = "Please enter Customer name"
            static let kCustomerEmail = "Please enter email"
            static let kCustomerPhoneNo = "Please enter phone number"
            static let kCustomerlatestUpdate = "Please enter latest update"
            static let kJobskill = "Please select Jobskill"
            static let kJobTitleEnter = "Please enter JobTitle"
            static let kJobDiscription = "Please enter Job Discription"
            
            
            
        }
    }
}

struct ApiConstants {
    struct key {
        static let document_name = "document_name"
        static let kwork_radius = "work_radius"
        static let kEmail = "email"
        static let ktrader_id = "trader_id"
        static let ktradee_id = "tradee_id"
        static let kpage_id = "page_id"
        
        static let kimage = "images"
        static let kimages = "images[]"
        static let kimagee = "image"
        
        static let kverification_id = "verification_id"
        
        static let kvalidation_id = "validation_id"
        static let kPassword = "password"
        static let kdevice_token = "device_token"
        static let kuser_type = "user_type"
        static let kuser_id = "user_id"
        static let kreport_by = "report_by"
        static let kreason = "reason"
        static let krepost_by = "repost_by"
        static let krepost_user_type = "repost_user_type"
        static let kcreate_post_type = "create_post_type"
        
        static let kdocument_type = "document_type"
        static let kpost_id = "post_id"
        static let kcomment = "comment"
        static let kreply = "reply"
        static let kcomment_id = "comment_id"
        static let kid = "id"
        static let kdocument = "document[]"
        static let ktitle = "tittle"
        static let kPage_tittle = "page_tittle"
        static let kdescription = "description"
        static let kskill_level = "skill_level"
        static let kyear_of_experience = "year_of_experience"
        static let kapprentice = "apprentice"
        static let kskill_needed = "skill_needed"
        static let ksuggested_skills = "suggested_skills"
        static let kprofileImage = "image"
        static let kfull_name = "full_name"
        static let kaddress = "address"
        static let kpostType = "post_type"
         static let kresource_id = "resource_id"
        
        
        static let kstory_id = "story_id"
        struct punch_in_out {
            static let kpunch_out_date = "punch_out_date"
            static let kpunch_out_time = "punch_out_time"
            static let klocation = "location"
            static let kresource_id = "resource_id"
            static let klat = "lat"
            static let klong = "long"
            static let kproject_id = "project_id"
            static let kpunch_in_date = "punch_in_date"
            static let kpunch_in_time = "punch_in_time"
        }
        struct credentialsKey {
            static let user_type = "user_type"
            static let provider = "provider"
            static let number = "number"
            static let expiry_date = "expiry_date"
            static let title = "tittle"
        }
        struct businessKey {
            static let kbusiness_name = "business_name"
            static let kABN_CAN = "ABN_CAN"
            static let kwebsite = "website"
            static let kbusiness_type = "business_type"
            static let kbusiness_experience = "business_experience"
            
        }
        struct registerConstants {
            static let kEmail = "email"
            static let kPassword = "password"
            static let kdevice_token = "device_token"
            static let kdevice_type = "device_type"
            static let kcountry_code = "country_code"
            static let kfull_name = "full_name"
            static let kmobile_number = "mobile_number"
        }
        struct createProject {
            static let kuser_id = "user_id"
            static let kproject_name = "project_name"
            static let kproject_type = "project_type"
            static let kproject_time = "project_time"
            static let kresource_required = "resource_required"
            static let kjob_time = "job_time"
            static let kproject_id = "project_id"
            static let kworking_skills = "working_skills"
            static let kjob_description = "job_description"
            static let kjob_type = "job_type"
            static let krequired_resources = "required_resources"
            static let kjob_post = "job_post"
            static let kuser_resource_list = "user_resource_list"
            static let kpostcode = "postcode"
            static let kaddress = "address"
            static let klatitude = "latitude"
            static let klongitude = "longitude"
            static let kdistance = "distance"
            static let kproject_start_time = "project_start_time"
            static let kproject_end_time = "project_end_time"
            static let kjob_start_time = "job_start_time"
            static let kjob_end_time = "job_end_time"
            static let kcustomer_name = "customer_name"
            static let kcustomer_phone_no = "customer_phone_no"
            static let kcustomer_email = "customer_email"
            static let klastest_updates = "lastest_updates"
            static let kresource = "resource"
            static let kskills = "skills"
            static let kdefine_rate = "define_rate"
            static let kpayment = "payment"
            static let kID = "lead_id"
            
            
            
        }
        
        struct ConnectionRequestkeys {
            static let ksender_id = "sender_id"
            static let kreceiver_id = "receiver_id"
        }
        
        struct ContactUs {
            static let kto = "to"
            static let kmessage = "message"
            static let ksubject = "subject"
        }
        
        struct NoticationKey {
            static let kprivacy_status = "privacy_status"
        }
        struct NotificationOnOff {
            static let   notify_on_connection =      "notify_on_connection"
            static let   notify_on_message = "notify_on_message"
            static let   notify_on_application = "notify_on_application"
            static let   notify_on_review = "notify_on_application"
            static let   notify_on_comment = "notify_on_comment"
            static let   notify_on_hour_adjust = "notify_on_hour_adjust"
            static let   notify_on_profile = "notify_on_profile"
            static let   notify_on_tradee_view = "notify_on_tradee_view"
            static let    notify_on_likes = "notify_on_likes"
            static let   notify_on_connection_accepted = "notify_on_connection_accepted"
            static let    notifiy_on_clock = "notifiy_on_clock"
            static let    notifiy_on_job_complete = "notifiy_on_job_complete"
        }
        struct chatMessage {
            static let ksearch = "search"
            static let ksender_id = "sender_id"
            static let kreceiver_id = "receiver_id"
            
            
            
        }
    }
}
