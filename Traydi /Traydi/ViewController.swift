//
//  ViewController.swift
//  Traydi
//
//  Created by mac on 09/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if AppSharedData.shared.isUserGuideSkipped == true{
            let sb = UIStoryboard(name: "Authentication", bundle: nil)
            let vc = sb.instantiateInitialViewController() // LoginViewController
            UIApplication.shared.windows[0].rootViewController = vc
        }else{
            let storyboardGuide = UIStoryboard.init(name: "Guide", bundle: nil)
            let vc = storyboardGuide.instantiateInitialViewController() // Guide01ViewController
            UIApplication.shared.windows[0].rootViewController = vc
        }
    }
}

