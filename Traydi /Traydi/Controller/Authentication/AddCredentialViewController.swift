//
//  AddCredentialViewController.swift
//  Traydi
//
//  Created by mac on 20/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit

protocol AddCredentialDelegate:NSObject {
    func actionCreadentialList(items:[CredentialModal])
}

class AddCredentialViewController: UIViewController , UITextViewDelegate, UITextFieldDelegate {
     @IBOutlet weak var scrollviewBG: UIScrollView!
    @IBOutlet weak var txtFdTitle: UITextField!
    @IBOutlet weak var txtFdProvider: UITextField!
    @IBOutlet weak var txtFdNumber: UITextField!
    @IBOutlet weak var txtFdDate: UITextField!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var constraintBackHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintImageStepHeight: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    var controllerType:Bool = false
    weak var delegate:AddCredentialDelegate?
     let datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNotifications()
        self.setupToHideKeyboardOnTapOnView()
        self.txtFdTitle.delegate = self
        self.txtFdProvider.delegate = self
        self.txtFdNumber.delegate = self
        self.txtFdDate.delegate = self
        self.txtFdTitle.layer.cornerRadius = 25
        self.txtFdTitle.layer.borderWidth = 2
        self.txtFdTitle.layer.borderColor = UIColor(displayP3Red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0).cgColor
      
        
        self.txtFdProvider.layer.cornerRadius = 25
        self.txtFdProvider.layer.borderWidth = 2
        self.txtFdProvider.layer.borderColor = UIColor(displayP3Red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0).cgColor
        
        self.txtFdNumber.layer.cornerRadius = 25
        self.txtFdNumber.layer.borderWidth = 2
        self.txtFdNumber.layer.borderColor = UIColor(displayP3Red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0).cgColor
        
        self.txtFdDate.layer.cornerRadius = 25
        self.txtFdDate.layer.borderWidth = 2
        self.txtFdDate.layer.borderColor = UIColor(displayP3Red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0).cgColor
        
        self.btnAdd.layer.cornerRadius = 25.0
        if AppSharedData.shared.isSeeker == true{
            self.lblTitle.text  = "Tradees"
        }else{
            //Traders
            self.lblTitle.text = "Traders"
        }
        if controllerType {
            lblTitle.text = "Edit"
            constraintBackHeight.constant = 0
            constraintImageStepHeight.constant = 0
            btnAdd.setTitle("Save", for: .normal)
           // let tabBarController = self.tabBarController as! CustomTabBarController
           // tabBarController.isHiddenTabBar(hidden: true)
        }
        showDatePicker()
        if controllerType{
            let custombar = self.tabBarController as! CustomTabBarController
            custombar.isHiddenTabBar(hidden: true)
        }
        
    }
   
    func showDatePicker(){
       //Formate Date
       datePicker.datePickerMode = .date

      //ToolBar
      let toolbar = UIToolbar();
      toolbar.sizeToFit()
      let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
     let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));

    toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)

     txtFdDate.inputAccessoryView = toolbar
     txtFdDate.inputView = datePicker

    }

     @objc func donedatePicker(){

      let formatter = DateFormatter()
      formatter.dateFormat = "yyyy/MM/dd"
      txtFdDate.text = formatter.string(from: datePicker.date)
      self.view.endEditing(true)
    }

    @objc func cancelDatePicker(){
       self.view.endEditing(true)
     }
    
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.dismiss(animated: true) {
            // Completion block
        }
    }
    
    //MARK:- Validations
    func validation() -> (message:String, status:Bool) {
        var message:String = ""
        var status:Bool = true
        if txtFdTitle.text!.isEmpty {
            message = UseCaseMessage.validation.empty.kTitle
            status = false
        }
        /*else if txtFdProvider.text!.isEmpty {
            message = UseCaseMessage.validation.empty.kProvider
            status = false
        } else if txtFdNumber.text!.isEmpty {
            message = UseCaseMessage.validation.empty.knumber
            status = false
        }*/
        else if txtFdDate.text!.isEmpty {
            message = UseCaseMessage.validation.empty.kDate
            status = false
        }
        return (message:message , status:status)
    }
  
    @IBAction func addBtnAction(_ sender: Any) {
        
        let valid = validation()
        
        if valid.status {
            APIManager.share.delegate = self
            let data = AppSignleHelper.shard.login
            var params = [String:Any]()
            params[ApiConstants.key.credentialsKey.user_type] = data?.userinfo?.user_type ?? ""
            params[ApiConstants.key.credentialsKey.title] = txtFdTitle.text
            params[ApiConstants.key.credentialsKey.number] = txtFdNumber.text
            params[ApiConstants.key.credentialsKey.expiry_date] = txtFdNumber.text
            params[ApiConstants.key.credentialsKey.provider] = txtFdProvider.text
           // params[ApiConstants.key.ktrader_id] =  data?.userinfo?.id ?? "0"
            if AppSharedData.shared.isSeeker == true{
                params[ApiConstants.key.ktradee_id] =  data?.userinfo?.id ?? "0"
             }else{
                params[ApiConstants.key.ktrader_id] =  data?.userinfo?.id ?? "0"
            }
            let apiAction = ApiAction.TradersAndTradees.kcredentials
            APIManager.share.performMaltipartPost(params: params, apiAction: apiAction)
        } else {
            Util.showAlertWithCallback(AppName, message: valid.message, isWithCancel: false)
        }
    }
    
    //******************************************************************
     // MARK:-  KEYBOARD NOTIFICATION METHODS
     //******************************************************************
     
     func registerNotifications() {
         NotificationCenter.default.addObserver(
             self,
             selector: #selector(self.keyboardWillShow),
             name: UIResponder.keyboardWillShowNotification,
             object: nil)
         
         NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
     }
     
     func unregisterNotifications() {
         NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
         NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
     }
     //UIKeyboardFrameBeginUserInfoKey
     @objc func keyboardWillShow(notification: NSNotification) {
         let userInfo = notification.userInfo ?? [:]
         let keyboardFrame = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
         let height = keyboardFrame.height + 150
         scrollviewBG.contentInset.bottom = height
         scrollviewBG.scrollIndicatorInsets.bottom = height
     }
     
     @objc func keyboardWillHide(notification: NSNotification) {
         scrollviewBG.contentInset.bottom = 0
         scrollviewBG.scrollIndicatorInsets.bottom = 0
         scrollviewBG.scrollRectToVisible(CGRect.zero, animated: true)
     }
     
     override func viewDidDisappear(_ animated: Bool) {
         super.viewDidDisappear(animated)
         self.unregisterNotifications()
         
     }
}

extension AddCredentialViewController:ApiManagerDelegate {
    func successResponse(response: Any, check: String) {
        Loader.hideLoader()
        if check == ApiAction.TradersAndTradees.kcredentials {
            guard let data = response as? Data else {
                return
            }
            guard   let workType = try? JSONDecoder().decode(LoginModal.self, from: data) else { return  }
        if workType.code == ApiResponseStatus.kAPI_Result_Success {
            AppSignleHelper.shard.login = workType
            Util.showAlertWithCallback(AppName, message: workType.message, isWithCancel: false) {
               if let loginViewController = self.navigationController?.viewControllers.first(where: { $0.isKind(of: LoginViewController.self) }) as? LoginViewController {
                   self.navigationController?.popToViewController(loginViewController, animated: true)
               }
            }
            }
            
        }
    }
    
    func errorResponse(error: Any, check: String) {
        guard let errormsg = error as? Error else {
            return
        }
        Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
    }
    
    
      func textFieldShouldReturn(_ textField: UITextField) -> Bool {
               if textField == txtFdTitle {
                   txtFdProvider.becomeFirstResponder()
               }else if textField == txtFdProvider{
                   txtFdNumber.becomeFirstResponder()
               }else if textField == txtFdNumber{
                   txtFdDate.becomeFirstResponder()
               }else{
                   textField.resignFirstResponder()
               }
               return true
           }
        
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
             let rawString = string
             let range1 = rawString.rangeOfCharacter(from: .whitespaces)
             if ((textField.text?.count)! == 0 && range1  != nil)
                 || ((textField.text?.count)! > 0  && range1 != nil && textField != txtFdTitle && textField != txtFdProvider  && textField != txtFdNumber )  {
                 return false
             }
             let length = (textField.text?.count)! + string.count - range.length
             
          
                   
                   if txtFdNumber == textField {
                       
                       let validCharacterSet = NSCharacterSet(charactersIn: "0123456789").inverted
                       let filter = string.components(separatedBy: validCharacterSet)
                       if filter.count == 1 {
                           
                           return (length > 30) ? false : true
                       } else {
                           return false
                       }
                   }else if textField == txtFdTitle || textField == txtFdProvider {
                       
                       let validCharacterSet = NSCharacterSet(charactersIn: APPConstants.NameAcceptableCharacter).inverted
                       let filter = string.components(separatedBy: validCharacterSet)
                       if filter.count == 1 {
                           
                           return (length > 30) ? false : true
                       } else {
                           return false
                       }
                   }
    //               else if textField == txtFdPassword || textField == txtConfirmPassword {
    //                   return (length > 40) ? false : true
    //               }
                   return true
             
             
         }
        
        
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            self.view.endEditing(true)
        }
        
}






