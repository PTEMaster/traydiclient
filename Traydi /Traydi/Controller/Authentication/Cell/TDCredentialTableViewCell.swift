//
//  TDCredentialTableViewCell.swift
//  Traydi
//
//  Created by mac on 02/03/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit

class TDCredentialTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblProvider: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
