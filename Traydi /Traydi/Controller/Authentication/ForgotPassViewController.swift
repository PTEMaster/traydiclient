//
//  ForgotPassViewController.swift
//  Traydi
//
//  Created by mac on 11/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit

class ForgotPassViewController: UIViewController {
    
    @IBOutlet weak var viewForgotPass: UIView!

    @IBOutlet weak var btnForgotPass: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewForgotPass.layer.shadowColor = UIColor.black.cgColor //UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 1).cgColor
        self.viewForgotPass.layer.shadowOpacity = 0.1
        self.viewForgotPass.layer.shadowOffset = .zero //CGSize(width: 3, height: 3)
        self.viewForgotPass.layer.shadowRadius = 20
        self.viewForgotPass.layer.cornerRadius = 15.0
        self.btnForgotPass.layer.cornerRadius = 25.0
        let custombar = self.tabBarController as? CustomTabBarController
        custombar?.isHiddenTabBar(hidden: true)
        
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}




