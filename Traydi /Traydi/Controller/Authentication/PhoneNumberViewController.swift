//
//  PhoneNumberViewController.swift
//  Traydi
//
//  Created by mac on 11/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//
import UIKit
class PhoneNumberViewController: UIViewController, TDCountryPickerDelegate {
   
    @IBOutlet weak var btnCountryCode: UIButton!
    @IBOutlet weak var tfPhoneNumber: UITextField!
    @IBOutlet weak var btnVerifyNumber: UIButton!
    @IBOutlet weak var viewPhoneNumber: UIView!
    var modelLoginModal : LoginModal?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewPhoneNumber.layer.shadowColor = UIColor.black.cgColor
        self.viewPhoneNumber.layer.shadowOpacity = 0.1
        self.viewPhoneNumber.layer.shadowOffset = .zero
        self.viewPhoneNumber.layer.shadowRadius = 20
        self.viewPhoneNumber.layer.cornerRadius = 15.0
        
        self.btnVerifyNumber.layer.cornerRadius = 25.0
        setData()
    }
    
    //Set data  from modelLoginModal get from registration page
    func setData(){
        let cCode = modelLoginModal?.userinfo?.country_code ?? ""
        btnVerifyNumber.setTitle( "+" + cCode, for: .normal)
        let mobile_number = modelLoginModal?.userinfo?.mobile_number
        tfPhoneNumber.text = mobile_number
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pushVerifyNumberSegueIdentifier" {
            //segue.destination
        }
    }
    @IBAction func areaCodeBtnAction(_ sender: Any) {
        let countryPicker = TDCountryPicker(style: .grouped)
        countryPicker.delegate = self
        
        // Display calling codes
        countryPicker.showCallingCodes = false
    
         let pickerNavigationController = UINavigationController(rootViewController: countryPicker)
        self.present(pickerNavigationController, animated: true, completion: nil)
    }
    
    @IBAction func actionVerifyNumber(_ sender: Any) {
        
    }
    
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func countryPicker(_ picker: TDCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        _ = picker.navigationController?.popToRootViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
       // let flag =  picker.getFlag(countryCode: code)
        btnCountryCode.setTitle(dialCode, for: .normal)
     //   btnRegister.setImage(flag, for: .normal)
    }
    
}
