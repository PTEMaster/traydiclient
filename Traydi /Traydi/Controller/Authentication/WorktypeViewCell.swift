//
//  WorktypeViewCell.swift
//  Traydi
//
//  Created by mac on 04/02/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit

class WorktypeViewCell: UITableViewCell {
    
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var lblTypeName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

      
    }

}
