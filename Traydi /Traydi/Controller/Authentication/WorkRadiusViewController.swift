//
//  WorkRadiusViewController.swift
//  Traydi
//
//  Created by mac on 20/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit
import GoogleMaps
//import GooglePlaces
//import SnapKit

class WorkRadiusViewController: UIViewController, LocationServiceDelegate, ApiManagerDelegate {
    
    
    
    @IBOutlet weak var scrollviewBG: UIScrollView!
    @IBOutlet weak var collectionViewWorkRadius: UICollectionView! //CustomPickerView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var lblLocationName: UILabel!
    @IBOutlet weak var lblArea: UILabel!
    @IBOutlet weak var lblRadius: UILabel!
    @IBOutlet weak var viewBgMapview: GMSMapView!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var constraintImageStep: NSLayoutConstraint!
    @IBOutlet weak var constraintBackHeight: NSLayoutConstraint!
    var isSelectRadius : Int?
    var latitude  : Double = 0.0
    var longitude  : Double = 0.0
    
    var controllerType:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
      //  registerNotifications()
      //  self.setupToHideKeyboardOnTapOnView()
        self.collectionViewWorkRadius.decelerationRate = UIScrollView.DecelerationRate.fast
        //71, 91
      //  self.scrollviewBG.contentSize = CGSize(width: self.view.bounds.width, height: 896)
        self.btnContinue.layer.cornerRadius = 25.0
        if AppSharedData.shared.isSeeker == true{
            self.lblTitle.text  = "Tradees"
        }else{
            //Traders
            self.lblTitle.text = "Traders"
        }
        if controllerType {
            lblTitle.text = "Edit"
            constraintBackHeight.constant = 0
            constraintImageStep.constant = 0
            btnContinue.setTitle("Save", for: .normal)
            let tabBarController = self.tabBarController as! CustomTabBarController
            tabBarController.isHiddenTabBar(hidden: true)
        }

        
        LocationSingleton.sharedInstance.delegate = self
        
        setMap()
//        if (appDelegate.shared.locManager?.location?.coordinate.latitude) != nil {
//        let latitude  = appDelegate.shared.locManager?.location?.coordinate.latitude
//            print(latitude as Any)
//        let longitude = appDelegate.shared.locManager?.location?.coordinate.longitude
//              print(longitude as Any)
//        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        let nav = self.navigationController?.navigationBar
        nav?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    

    
    func setMap(){
        self.viewBgMapview.clear()
         if controllerType {
            isSelectRadius =   Int(AppSignleHelper.shard.login.userinfo?.work_radius ?? "0")
            
            searchBar.text =    AppSignleHelper.shard.login.userinfo?.postalcode
         
            if  let la = Double(AppSignleHelper.shard.login.userinfo!.latitude!) {
                  latitude = la
                    }
            
            if  let la = Double(AppSignleHelper.shard.login.userinfo!.longitude!) {
                  longitude = la
                    }
            
                let camera = GMSCameraPosition.camera(withLatitude: latitude , longitude: longitude, zoom: 16.0)
                self.viewBgMapview.settings.compassButton = true
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: latitude , longitude: longitude)
                marker.icon = UIImage(named: "locationpin")//imgLocation
                marker.map = viewBgMapview
                self.viewBgMapview.animate(to: camera)
                //get address from latitude using geocoder
                geocode(latitude: latitude, longitude: longitude) { (address, locality ,error) in
                    if error == nil{
                        self.lblLocationName.text = address
                        self.lblArea.text  = locality
                    }
                }
            
         }else {
      
         print(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude)
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
           
                        guard let currentLocation = LocationSingleton.sharedInstance.locationManager?.location?.coordinate else {
                            return
                        }
            latitude = currentLocation.latitude
            longitude = currentLocation.longitude
            
            let camera = GMSCameraPosition.camera(withLatitude: currentLocation.latitude , longitude: currentLocation.longitude, zoom: 16.0)
            self.viewBgMapview.settings.compassButton = true
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: currentLocation.latitude , longitude: currentLocation.longitude)
            marker.icon = UIImage(named: "locationpin")//imgLocation
            marker.map = viewBgMapview
            self.viewBgMapview.animate(to: camera)
        
            
            //get address from latitude using geocoder
            geocode(latitude: currentLocation.latitude, longitude: currentLocation.longitude) { (address, locality ,error) in
                if error == nil{
                    self.lblLocationName.text = address
                    self.lblArea.text  = locality
                }
            }
            }
        }
    }
    
    
    func tracingLocation(currentLocation: CLLocation){
       
        print( currentLocation.coordinate.latitude)
    }
    func tracingLocationDidFailWithError(error: NSError) {
        print(error)
    }
    
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionContinue(_ sender: Any) {
         APIManager.share.delegate = self
        
             let valid = Validation()
             if valid.status {
                let data = AppSignleHelper.shard.login
                var params = [String:Any]()
              
                params[ApiConstants.key.createProject.kdistance] = isSelectRadius
                params[ApiAction.TradersAndTradees.kwork_radius] = isSelectRadius
                params[ApiAction.TradersAndTradees.kpostalcode] = searchBar.text
             //    params[ApiConstants.key.ktrader_id] =  data?.userinfo?.id
                if AppSharedData.shared.isSeeker == true{
                    params[ApiConstants.key.ktradee_id] =  data?.userinfo?.id ?? "0"
                 }else{
                    params[ApiConstants.key.ktrader_id] =  data?.userinfo?.id ?? "0"
                }
                params[ApiConstants.key.createProject.klatitude] = latitude
                 params[ApiConstants.key.createProject.klongitude] = longitude
                let apiAction = ApiAction.TradersAndTradees.kwork_radius
                
                APIManager.share.performMaltipartPost(params: params, apiAction: apiAction)
                
            }else {
                Util.showAlertWithCallback(AppName, message: valid.message, isWithCancel: false)
            }
          //  self.performSegue(withIdentifier: "pushToCredentialListViewController", sender: self)
    }
    
  /*  @IBAction func actionOpenMap(_ sender: Any) {
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        acController.modalPresentationStyle = .overCurrentContext
        acController.modalTransitionStyle = .crossDissolve
        present(acController, animated: true, completion: nil)
    }
    //MARK:- Autocomplete map delegate
      func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
          logInfo("Place name: \(place.name)")
          logInfo("Place address: \(String(describing: place.formattedAddress))")
          logInfo("Place attributions: \(String(describing: place.attributions))")
        //  houseAddress = place.formattedAddress!
        //  houseLongitude = String(place.coordinate.longitude)
        //  houseLatitude = String(place.coordinate.latitude)
         // self.searchButton.setTitle(place.formattedAddress, for: .normal)
         // self.updateMap(lattitude: houseLatitude, longitude: houseLongitude, address: houseAddress)
          dismiss(animated: true, completion: nil)
      }
      
      func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
          // TODO: handle the error.
          logInfo("Error: \(error)")
          dismiss(animated: true, completion: nil)
      }
      
      // User cancelled the operation.
      func wasCancelled(_ viewController: GMSAutocompleteViewController) {
          logInfo("Autocomplete was cancelled.")
          dismiss(animated: true, completion: nil)
      }*/
    // custom log
    func logInfo(_ message: String, file: String = #file, function: String = #function, line: Int = #line, column: Int = #column) {
            print("\(function): \(line): \(message)")
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func Validation() -> (message:String, status:Bool) {
          var msg:String = ""
          var status:Bool = true
          if searchBar.text!.isEmpty {
              msg = UseCaseMessage.validation.empty.kPostcode
              status = false
          }
         else if isSelectRadius == nil {
              msg = UseCaseMessage.validation.empty.kdistance
            status = false
          }
          return (message:msg , status:status)
      }
}

extension WorkRadiusViewController: UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 1
    }
    
 
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionViewWorkRadius.dequeueReusableCell(withReuseIdentifier: "RadiusCollectionCell", for: indexPath) as! RadiusCollectionCell
          //  cell = RadiusCollectionCell(frame: CGRect(x: 0, y: 0, width: 71, height: 91))
            cell.lblKM.text = "\(indexPath.section)"
        if indexPath.section == isSelectRadius{
                       cell.lblKM.backgroundColor = appColor
                   }else{
                       cell.lblKM.backgroundColor = .clear
                   }
       
        return cell
    }
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = self.collectionViewWorkRadius.dequeueReusableCell(withReuseIdentifier: "RadiusCollectionCell", for: indexPath) as! RadiusCollectionCell
        isSelectRadius = indexPath.section
        lblRadius.text = "\(indexPath.section) KM"

        collectionViewWorkRadius.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }

   
   

   
  
  /* func snapToNearestCell(scrollView: UIScrollView) {
         let middlePoint = Int(scrollView.contentOffset.x + UIScreen.main.bounds.width / 2)
         if let indexPath = self.collectionViewWorkRadius.indexPathForItem(at: CGPoint(x: middlePoint, y: 0)) {
             isSelectRadius = indexPath.section
            lblRadius.text = "\(indexPath.section) KM"
            self.collectionViewWorkRadius.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
         }
    }
     
     
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.snapToNearestCell(scrollView: scrollView)
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.snapToNearestCell(scrollView: scrollView)
    }*/
   
    
   
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 20
    }
}

extension WorkRadiusViewController : UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print(searchBar.text)
        searchBar.resignFirstResponder()
    }
    
  
    
    func geocode(latitude: Double, longitude: Double, completion: @escaping (_ placemark: String?, _ locality: String?, _ error: Error?) -> Void)  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { placemark, error in
            guard let placemark = placemark, error == nil else {
                completion(nil, nil ,error)
                
                return
            }
            let pm = placemark
              if pm.count > 0 {
                let pm = placemark[0]
//                print(pm.country)
//                print(pm.locality)
//                print(pm.subLocality)
//                print(pm.thoroughfare)
//                print(pm.postalCode)
//                print(pm.subThoroughfare)
                                
                  var addressString : String = ""
                  if pm.subLocality != nil {
                      addressString = addressString + pm.subLocality! + ", "
                  }
                  if pm.thoroughfare != nil {
                      addressString = addressString + pm.thoroughfare! + ", "
                  }
                  if pm.locality != nil {
                      addressString = addressString + pm.locality! + ", "
                  }
                  if pm.country != nil {
                      addressString = addressString + pm.country! + ", "
                  }
                  if pm.postalCode != nil {
                      addressString = addressString + pm.postalCode! + " "
                  }

                completion(addressString, pm.locality,nil)
            }
        }
    }
    
    
    func errorResponse(error: Any, check: String) {
        Loader.hideLoader()
        guard let errormsg = error as? Error else {
            return
        }
        Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
    }
    
    func successResponse(response: Any, check: String) {
        Loader.hideLoader()
        guard let data = response as? Data else {
            return
        }
        if check == ApiAction.TradersAndTradees.kwork_radius {
            guard   let workType = try? JSONDecoder().decode(LoginModal.self, from: data) else { return  }
            //  guard let loginModal = try? JSONDecoder().decode(LoginModal.self, from:data) else { return  }
            
            if workType.code == ApiResponseStatus.kAPI_Result_Success {
                //set data in local
                AppSignleHelper.shard.login = workType
                UserDefaults.standard.setUserData(value: data)
                
                    Util.showAlertWithCallback(AppName, message: workType.message, isWithCancel: false) {
                        if self.controllerType {
                            self.navigationController?.popViewController(animated: true)
                        } else {
                        let vc = AddCredentialViewController.instance(.authentication) as! AddCredentialViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                        //  self.performSegue(withIdentifier: "pushToWorkTypeViewController", sender: self)
                    }
                }
            }
        }
    }
    
    //******************************************************************
    // MARK:-  KEYBOARD NOTIFICATION METHODS
    //******************************************************************
    
    func registerNotifications() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func unregisterNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    //UIKeyboardFrameBeginUserInfoKey
    @objc func keyboardWillShow(notification: NSNotification) {
        let userInfo = notification.userInfo ?? [:]
        let keyboardFrame = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let height = keyboardFrame.height + 150
        scrollviewBG.contentInset.bottom = height
        scrollviewBG.scrollIndicatorInsets.bottom = height
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        scrollviewBG.contentInset.bottom = 0
        scrollviewBG.scrollIndicatorInsets.bottom = 0
        scrollviewBG.scrollRectToVisible(CGRect.zero, animated: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.unregisterNotifications()
        
    }
    
}



