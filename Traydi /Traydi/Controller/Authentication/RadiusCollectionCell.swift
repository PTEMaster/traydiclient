//
//  RadiusCollectionCell.swift
//  Traydi
//
//  Created by mac on 20/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit
class RadiusCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblKM: UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
