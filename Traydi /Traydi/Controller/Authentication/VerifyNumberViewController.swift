//
//  VerifyNumberViewController.swift
//  Traydi
//
//  Created by mac on 12/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit
class VerifyNumberViewController: UIViewController {
    @IBOutlet weak var scrollViewBg: UIScrollView!
    
    @IBOutlet weak var txtFd1: UITextField!
    @IBOutlet weak var txtFd2: UITextField!
    @IBOutlet weak var txtFd3: UITextField!
    @IBOutlet weak var txtFd4: UITextField!
    
    @IBOutlet weak var lblDash4: UILabel!
    @IBOutlet weak var lblDash3: UILabel!
    @IBOutlet weak var lblDash2: UILabel!
    @IBOutlet weak var lblDash1: UILabel!
    @IBOutlet weak var lblMsgWithNumber: UILabel!
    @IBOutlet weak var verifyBtn: UIButton!
    @IBOutlet weak var viewSuccessfullyVerified: UIView!
    @IBOutlet weak var viewSuccessfullBg: UIView!
    
    var modelLoginModal : LoginModal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNotifications()
        setupData()
        self.verifyBtn.layer.cornerRadius = 25.0

        self.viewSuccessfullBg.addShadowAllSide()
         lblDash1.backgroundColor = appColor
        txtFd1.becomeFirstResponder()
    }
    
    func setupData(){
        //Please enter the 4 digit code sent via SMS to +61424425445
        let cCode  = modelLoginModal?.userinfo?.country_code ?? ""
        let mobile_number  = modelLoginModal?.userinfo?.mobile_number ?? ""
        lblMsgWithNumber.text = "Please enter the 4 digit code sent via SMS to " + cCode + mobile_number
    }
    func alltextFieldGrayColor(){
        lblDash1.backgroundColor = .lightGray
        lblDash2.backgroundColor = .lightGray
        lblDash3.backgroundColor = .lightGray
        lblDash4.backgroundColor = .lightGray
        
    }

    
    @IBAction func backBtnAction(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
    }
    @IBAction func resendBtnAction(_ sender: Any) {
    
    }
    @IBAction func verifyBtnAction(_ sender: Any) {
        
        self.viewSuccessfullyVerified.frame = self.view.frame
          self.view.addSubview(self.viewSuccessfullyVerified)
        
       
    }
    @IBAction func backgroundClicked(_ sender: Any) {
        
        self.viewSuccessfullyVerified.removeFromSuperview()
        let vc = ProfilePictureViewController.instance(.authentication) as! ProfilePictureViewController
    //    vc.modelLoginModal =  loginModal
        self.navigationController?.pushViewController(vc, animated: true)
     
    }
    
}
extension VerifyNumberViewController:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        let strr:NSString = textField.text! as NSString
       let str2 = strr.replacingCharacters(in: range, with: string)  as NSString
        let length = (textField.text?.count)! + string.count - range.length
        alltextFieldGrayColor()
        if textField == txtFd1 {
            if str2 == ""{
                txtFd1.text = ""
                lblDash1.backgroundColor = appColor
                return false
            }
            let str = String(String(str2).last!)
            txtFd1.text = ""
            txtFd1.text =  str//String(str)
            txtFd2.becomeFirstResponder()
            lblDash2.backgroundColor = appColor
            return false
        }
       if textField == txtFd2 {
           if str2 == ""{
               txtFd2.text = ""
               txtFd1.becomeFirstResponder()
            lblDash1.backgroundColor = appColor
               return false
           }
           let str = String(String(str2).last!)
           txtFd2.text = ""
           txtFd2.text =  str//String(str)
           txtFd3.becomeFirstResponder()
           lblDash3.backgroundColor = appColor
           return false
       }
        if textField == txtFd3 {
            if str2 == ""{
                txtFd3.text = ""
                txtFd2.becomeFirstResponder()
                lblDash2.backgroundColor = appColor
                return false
            }
            let str = String(String(str2).last!)
            txtFd3.text = ""
            txtFd3.text =  str//String(str)
            txtFd4.becomeFirstResponder()
            lblDash4.backgroundColor = appColor
            return false
        }
        if textField == txtFd4 {
            if str2 == ""{
                txtFd4.text = ""
                txtFd3.becomeFirstResponder()
                lblDash3.backgroundColor = appColor
                return false
            }
            let str = String(String(str2).last!)
            txtFd4.text = ""
            txtFd4.text =  str//String(str)
            textField.becomeFirstResponder()
            lblDash4.backgroundColor = appColor
            return false
        }
        
        return false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        if textField == txtFd4{
            textField.resignFirstResponder()
        }
        return true
    }
   
    
    func registerNotifications() {
          NotificationCenter.default.addObserver(
              self,
              selector: #selector(self.keyboardWillShow),
              name: UIResponder.keyboardWillShowNotification,
              object: nil)
          
          NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
      }
      
      func unregisterNotifications() {
          NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
          NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
      }
      //UIKeyboardFrameBeginUserInfoKey
      @objc func keyboardWillShow(notification: NSNotification) {
          let userInfo = notification.userInfo ?? [:]
          let keyboardFrame = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
          let height = keyboardFrame.height + 150
          scrollViewBg.contentInset.bottom = height
          scrollViewBg.scrollIndicatorInsets.bottom = height
      }
      
      @objc func keyboardWillHide(notification: NSNotification) {
          scrollViewBg.contentInset.bottom = 0
          scrollViewBg.scrollIndicatorInsets.bottom = 0
          scrollViewBg.scrollRectToVisible(CGRect.zero, animated: true)
      }
      
      override func viewDidDisappear(_ animated: Bool) {
          super.viewDidDisappear(animated)
          self.unregisterNotifications()
          
      }
}
