//
//  HomeSearchViewController.swift
//  Traydi
//
//  Created by mac on 23/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit
class HomeSearchViewController: UIViewController {
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var txtFSearch: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewHeader.layer.cornerRadius = 15.0
        self.viewHeader.clipsToBounds = true
        self.viewHeader.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        self.setupNavBar()
    }
    
    func setupNavBar() {
        let searchBarController = UISearchController(searchResultsController: nil)
        self.navigationItem.searchController = searchBarController
        self.navigationItem.hidesSearchBarWhenScrolling = false
   
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.dismiss(animated: true) {
            // completion block
        }
    }
}
