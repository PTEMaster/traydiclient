//
//  FeedViewCell.swift
//  Traydi
//
//  Created by mac on 25/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//
enum FeedViewCellType {
    case Text
    case TextWithImage
    case Repost
}

import UIKit

extension String {
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.height
    }
}
 protocol FeedViewCellDelegate: NSObject{
    func feedViewCell(cell:UITableViewCell)
    func feedViewCellWillComment(cell: UITableViewCell)
    func feedViewCellLikePost(cell: UITableViewCell)
    func feedViewControllerMenu(Cell: UITableViewCell , sender:UIButton)
    func feedViewPlayVideo(cell: UITableViewCell)
    func feedViewShareHeader(myIndex: Int)
    func feedViewLikeHeader(myIndex: Int)
}

class FeedViewCell: UITableViewCell {
   // @IBOutlet weak var textHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var constraintImgFeedHeight: NSLayoutConstraint!
    @IBOutlet weak var imgViewFeed: UIImageView!
    @IBOutlet weak var btnMoreOption: UIButton!
    @IBOutlet weak var constHeightView: NSLayoutConstraint!
    @IBOutlet weak var viewDetail: UIView!
    
  //  @IBOutlet weak var viewBg: UIImageView!
    
    @IBOutlet weak var imgProfile: TYImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblFeedText: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblcomment: UILabel!
     @IBOutlet weak var imgLike: UIImageView!
    var myIndex : Int = 0
    weak var delegate:FeedViewCellDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
      //  self.viewBg.layer.cornerRadius = 10.0
      //  self.viewBg.layer.shadowColor = UIColor.lightGray.cgColor
      //  self.viewBg.layer.shadowRadius = 8.0
      //  self.viewBg.layer.shadowOpacity = 0.5
      //  self.viewBg.layer.shadowOffset = .zero
        
    }
    @IBAction func moreOptionBtnAction(_ sender: UIButton) {
        print("MoreOptionBtnAction")
        delegate?.feedViewControllerMenu(Cell: self, sender: sender)
    }
    @IBAction func deleteBtnAction(_ sender: Any) {
         print("DeleteBtnAction")
    }
    
    @IBAction func editBtnAction(_ sender: Any) {
         print("EditBtnAction")
    }
    @IBAction func likeBtnAction(_ sender: Any) {
         print("LikeBtnAction")
        if delegate != nil{
          //  delegate?.feedViewCellLikePost(cell: self)
            delegate?.feedViewLikeHeader(myIndex: myIndex)
        }
    }
    @IBAction func commentsBtnAction(_ sender: Any) {
        if delegate != nil{
            delegate?.feedViewCellWillComment(cell: self)
        }
    }
    @IBAction func shareBtnAction(_ sender: Any) {
        if delegate != nil{
         //   delegate?.feedViewCell(cell: self)
            delegate?.feedViewShareHeader(myIndex: myIndex)
            
        }
    }
    static func heightForCellOf(type:FeedViewCellType, text:String?, width:CGFloat)-> Int{
        if type == .Text{
            let hText = text?.heightWithConstrainedWidth(width: width - 40, font: UIFont.systemFont(ofSize: 17))
            return 125 + Int(hText!)
        }
        return 80
    }
}

