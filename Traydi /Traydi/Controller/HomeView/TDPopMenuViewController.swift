//
//  TDPopMenuViewController.swift
//  Traydi
//
//  Created by mac on 16/01/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit
protocol HomeListingDeleteDelegate {
    func deleteIndex(indexx: IndexPath , buttontag:Int)
}

class TDPopMenuViewController: UIViewController {
    
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnRepost: UIButton!
    
    var typeButton = ""
    var dictPostlist : Postinfo?
    var  myIndex :IndexPath?
    var delegate: HomeListingDeleteDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if typeButton == "Feeds"{
           btnMenu.isHidden = false
           btnEdit.setTitle("Report", for: .normal)
           btnDelete.setTitle("Turn off notification", for: .normal)
           btnMenu.setTitle("Follow", for: .normal)
            btnEdit.setImage(UIImage(named:"report1"), for: .normal)
           btnDelete.setImage(#imageLiteral(resourceName: "notify_on"), for: .normal)
           btnMenu.setImage(#imageLiteral(resourceName: "follow_active"), for: .normal)
            btnEdit.tag = 111
            btnDelete.tag = 222
            btnMenu.tag = 333
            btnRepost.tag = 666
        }else{
            btnEdit.tag = 444
            btnDelete.tag = 555
           btnEdit.setTitle("Edit", for: .normal)
           btnDelete.setTitle("Delete", for: .normal)
           btnMenu.isHidden = true
            btnRepost.isHidden = true
           btnEdit.setImage(#imageLiteral(resourceName: "postedit"), for: .normal)
           btnDelete.setImage(#imageLiteral(resourceName: "postdelete"), for: .normal)
       
        }
    }
    
    
    
    @IBAction func actionEdit(_ sender: UIButton ) {
        if btnEdit.tag == 444{
            self.delegate?.deleteIndex(indexx: self.myIndex!, buttontag: sender.tag)
        }else{
            //111
            self.delegate?.deleteIndex(indexx: self.myIndex!, buttontag: sender.tag)
        }
        
    }
    
    @IBAction func actionDelete(_ sender: UIButton) {
        
        // FIXME: HIMANSHU PAL delete own post
        if btnDelete.tag == 555{
            var params = [String:Any]()
              params["post_id"] = dictPostlist?.id
              let apiAction = ApiAction.TradersAndTradees.kdelete_post
             // print(params)
             // print(apiAction)
              APIManager.share.performPostApiHandler(params: params, apiAction: apiAction, successCompletionHandler: { (response) in
                  let likedata = response.result.value as? [String:Any]
                  self.delegate?.deleteIndex(indexx: self.myIndex!, buttontag: 0)
                  
              }) { (error) in
               Util.showAlertWithCallback(AppName, message: error.localizedDescription , isWithCancel: false)
              }
        }else{
            //222
            self.delegate?.deleteIndex(indexx: self.myIndex!, buttontag: sender.tag)
            
            
        }
  

       //
    }
    
    @IBAction func actionMenu(_ sender: UIButton ) {
        ///333
        self.delegate?.deleteIndex(indexx: self.myIndex!, buttontag: sender.tag)
    }
    
    
    @IBAction func actionRepost(_ sender: UIButton) {
        //666
        self.delegate?.deleteIndex(indexx: self.myIndex!, buttontag: sender.tag)
    }
    

}
