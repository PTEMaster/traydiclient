//
//  TDPublicPageViewController.swift
//  Traydi
//
//  Created by mac on 27/01/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit

class TDPublicPageViewController: TDBaseViewController {

    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var viewMember: UIView!
    @IBOutlet weak var actionCreatePost: UIButton!
    @IBOutlet weak var tablePage: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnfollowing: UIButton!
    
   // @IBOutlet weak var tableHeightConstraints: NSLayoutConstraint!
    
    var pages:DataFetcherPageDetails?
    var pageData:DataFetcherPagesModal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        viewProfile.layer.cornerRadius = 20
        viewProfile.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        viewMember.layer.cornerRadius = 20
        viewMember.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
        let custombar = self.tabBarController as! CustomTabBarController
        custombar.isHiddenTabBar(hidden: true)
        self.isSetupPages_detailsApiCall()
        tablePage.register(UINib(nibName: "TDFeedSharePostTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "TDFeedSharePostTableViewCell")
       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillLayoutSubviews() {
       // tableHeightConstraints.constant = self.tablePage.contentSize.height
      //  view.needsUpdateConstraints()
    }
    
    @IBAction func actionChat(_ sender: Any) {
        let vc = TDChattingViewController.instance(.chatting)
        self.navigationController?.pushViewController(vc, animated: true)
    }
   
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionCreatePost(_ sender: Any) {
        self.tabBarController?.selectedIndex = 2
    }
    @IBAction func actionFollowed(_ sender: Any) {
        var params = [String:Any]()
        let loginUser = AppSignleHelper.shard.login.userinfo
        params[ApiConstants.key.ConnectionRequestkeys.ksender_id] = pageData?.id
        params[ApiConstants.key.ConnectionRequestkeys.kreceiver_id] = loginUser?.id
        let apiAction = ApiAction.TradersAndTradees.kfollow
        
        APIManager.share.performPostApiHandler(params: params, apiAction: apiAction, successCompletionHandler: { (response) in
            let response = response.result.value as! [String:Any]
            
            if response[ApiResponseStatus.kcode] as! String == ApiResponseStatus.kAPI_Result_Success {
                self.btnfollowing.setTitle("following", for: .normal)
            } else {
                self.btnfollowing.setTitle("followed", for: .normal)
            }
        }) { (error) in
            Util.showAlertWithCallback(AppName, message: error.localizedDescription, isWithCancel: false)
        }
    }
    
    
    
    func isSetupPages_detailsApiCall() {
        var params = [String:Any]()
        params[ApiConstants.key.kuser_id] = AppSignleHelper.shard.login.userinfo?.id ?? "1"
        params[ApiConstants.key.kpage_id] = pageData!.id
        let apiAction = ApiAction.TradersAndTradees.kpages_details
        APIManager.share.delegate = self
        APIManager.share.performMaltipartPost(params: params, apiAction: apiAction)
    }
}

extension TDPublicPageViewController:UITableViewDelegate , UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pages?.post!.count ?? 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TDFeedSharePostTableViewCell", for: indexPath) as! TDFeedSharePostTableViewCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       // tableHeightConstraints.constant = tableView.contentSize.height
      //  view.needsUpdateConstraints()
    }
    
  
}


extension TDPublicPageViewController: ApiManagerDelegate {
    
    func successResponse(response: Any, check: String) {
        guard let data = response as? Data else {
            return
        }
        
        if check == ApiAction.TradersAndTradees.kpages_details {
            do {
                let resultObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                let code  = resultObject[ApiResponseStatus.kcode] as? String
                let message = resultObject[ApiResponseStatus.kMessage] as? String
                if code == ApiResponseStatus.kAPI_Result_Success {
                    let pagesInfo = DataFetcherPageDetails(parameters: resultObject["pages"] as! [String : Any])
                    self.lblTitle.text = pagesInfo.page_tittle
                    self.pages = pagesInfo
                    self.tablePage.reloadData()
                } else {
                    Util.showAlertWithCallback(AppName, message: message, isWithCancel: false)
                }
            } catch {
                Util.showAlertWithCallback(AppName, message: "Unable to parse JSON response", isWithCancel: false)
            }
        }
    }
    
    func errorResponse(error: Any, check: String) {
        Loader.hideLoader()
        guard let errormsg = error as? Error else {
            return
        }
        Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
    }
}
