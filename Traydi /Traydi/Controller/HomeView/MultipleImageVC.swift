//
//  MultipleImageVC.swift
//  Traydi
//
//  Created by mac on 24/09/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//  MultipleImageVC


import UIKit
import  SDWebImage
class MultipleImageVC: UIViewController {
   // @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var tableViewComments: UITableView!
    var postComment: Postinfo?
  //  var arrComments = [Comment]()
    var isImages:Bool = true
    var myIndex : IndexPath?
    var isReplyToComment  : Bool = false
    var replyToIndex : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tabBarController = self.tabBarController as! CustomTabBarController
                      tabBarController.isHiddenTabBar(hidden: true)
        tableViewComments.sectionHeaderHeight = UITableView.automaticDimension
        
          //multiple image
            // FIXME: HIMANSHU PAL
            tableViewComments.register(UINib(nibName: "FeedViewCell", bundle: Bundle.main), forCellReuseIdentifier: "FeedViewCell")
            tableViewComments.scrollToTop(animated: true)
        self.tableViewComments.remembersLastFocusedIndexPath = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       tableViewComments.reloadData()
       //
    }
    
 
    
    
}

extension MultipleImageVC: UITableViewDataSource, UITableViewDelegate {
  
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FeedViewCell")  as! FeedViewCell
            // cell.delegate = self
            let objPost = postComment?.image?[section]
            cell.lblTitle.text = postComment!.full_name
            cell.lblTime.text = postComment!.created_at
             cell.delegate = self
            cell.myIndex  = section
             cell.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
             cell.imgProfile.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + postComment!.profile_image!), placeholderImage: UIImage(named: "camerawhite"))
            if section == 0 {
                 cell.viewDetail.isHidden = false
                 cell.constHeightView.constant = 62
                cell.lblFeedText.text = postComment!.tittle
            } else {
                cell.viewDetail.isHidden = true
                cell.constHeightView.constant = 0
                cell.lblFeedText.text = ""
            }
            
            if (objPost?.image!.isEmpty)! {
                cell.constraintImgFeedHeight.constant = 0
            } else {
                cell.constraintImgFeedHeight.constant = 170
                cell.imgViewFeed.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
                cell.imgViewFeed.sd_setImage(with: URL(string: ImagesUrl.basePostUri + (objPost?.image!)!), placeholderImage: UIImage(named: "camerawhite"))
            }
                        //like button
            if  postComment?.is_like  == "1"{
                   cell.imgLike.image = UIImage(named: "likepost")
               }else{
                    cell.imgLike.image = UIImage(named: "likecomments")
               }
            if postComment?.total_like == "0"{
                cell.lblLike.text = postComment?.total_like ?? "0" + " like"
               }
            else if postComment?.total_like == "1"{
                cell.lblLike.text = postComment?.total_like ?? "1" + " like"
               }else{
                cell.lblLike.text = postComment?.total_like ?? "2" + " likes"
               }
            
            //comment
            if postComment?.comments?.count == 0{
                cell.lblcomment.text =   "\(postComment?.comments?.count ?? 0) comment"
               }else if postComment?.comments?.count == 1{
                   cell.lblcomment.text = "\(postComment?.comments?.count ?? 1) comment"
               }
            else{
                   cell.lblcomment.text = "\(postComment?.comments?.count ?? 1) comments"
               }
            return cell
    }
    

    

    func numberOfSections(in tableView: UITableView) -> Int {
                return postComment?.image?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TDCommentTableViewCell")  as! TDCommentTableViewCell
            let dict = postComment?.comments?[indexPath.row]
            cell.lblComment.text = dict!.comment
            return cell
        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
             let cell = tableView.dequeueReusableCell(withIdentifier: "FeedViewCell")  as! FeedViewCell
            let lableHeight = cell.lblcomment.frame.size.height
            
            if section == 0 {
                return cell.frame.size.height
            } else {
              return cell.frame.size.height - lableHeight
            }
    }
    private func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    

    
    
    
    
}

extension MultipleImageVC: FeedViewCellDelegate{
    func feedViewLikeHeader(myIndex: Int) {
       
        
        let user_id = AppSignleHelper.shard.login.userinfo?.id
        let likeData = postComment?.id
        
        var params = [String:Any]()
        params["user_id"] = user_id ?? "0"
        params["post_id"] = likeData ?? "0"
        
        let apiAction = ApiAction.TradersAndTradees.kLikePost
        APIManager.share.performPostApiHandler(params: params, apiAction: apiAction, successCompletionHandler: { (response) in
            let likedata = response.result.value as? [String:Any]
            let likeResponse = LikeModel(parameters:likedata!)
            if  likeResponse.liked  == 1{
                //likepost
                self.postComment?.is_like = "1"
               
            }else{
                 self.postComment?.is_like = "0"
            }
             self.postComment?.total_like = likeResponse.likes
            self.tableViewComments.reloadData()
            

        }) { (error) in
         Util.showAlertWithCallback(AppName, message: error.localizedDescription , isWithCancel: false)
        }
        
    }
    
    func feedViewCell(cell: UITableViewCell) {
        
    }
    
   
    func feedViewShareHeader(myIndex: Int) {
             print(myIndex)
        let objPost = postComment?.image?[myIndex]
             
        print(ImagesUrl.basePostUri + (objPost?.image!)!)
             
        let someText:String = postComment?.tittle ?? ""
        let objectsToShare:URL = URL(string: ImagesUrl.basePostUri + (objPost?.image!)!)!
             let sharedObjects:[AnyObject] = [objectsToShare as AnyObject,someText as AnyObject]
             
             let activityViewController = UIActivityViewController(activityItems : sharedObjects, applicationActivities: nil)
             activityViewController.popoverPresentationController?.sourceView = self.view

             activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook,UIActivity.ActivityType.postToTwitter,UIActivity.ActivityType.mail]

             self.present(activityViewController, animated: true, completion: nil)
        
    }
    

    
    func feedViewCellWillComment(cell: UITableViewCell) {
        let vc =  CommentViewController.instance(.home) as! CommentViewController
               vc.postComment = postComment
               self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func feedViewCellLikePost(cell: UITableViewCell) {}
  /*  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
         let objPost = arrPostlist[indexPath.row]
         if objPost.post_type  == "" {
             
         } else {
             guard let cell = cell as? FeedViewMultipleCell else { return }
             cell.lblLike.text = objPost.total_like! + " Like"
         }
     }*/
    
    func feedViewControllerMenu(Cell: UITableViewCell, sender: UIButton) {
        
    }
    
    func feedViewPlayVideo(cell: UITableViewCell) {
        
    }
    
    
}







