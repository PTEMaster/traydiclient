//
//  TDChattingRecevierTextTableViewCell.swift
//  Traydi
//
//  Created by mac on 16/01/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit


protocol UploadImageDelegate: NSObject {
    func showImage(myIndex :Int)
}
class UploadImageCell: UITableViewCell {

    @IBOutlet weak var imgUploaded: TYImageView!
     weak var delegate:UploadImageDelegate?
     var  myIndex :Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func actionOpenImageInZoom(_ sender: Any) {
        self.delegate?.showImage(myIndex: myIndex ?? 0)
    }
    
}
