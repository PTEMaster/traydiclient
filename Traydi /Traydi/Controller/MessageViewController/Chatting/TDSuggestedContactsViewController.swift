//
//  TDSuggestedContactsViewController.swift
//  Traydi
//
//  Created by mac on 06/02/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit

class TDSuggestedContactsViewController: TDBaseViewController {
    @IBOutlet weak var tableSuggested: UITableView!
    @IBOutlet weak var viewHeader: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.3
        self.rightandLeftBottmCornerRadius(view: self.viewHeader)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension TDSuggestedContactsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "identifierSuggested", for: indexPath)
        return cell
    }
}
