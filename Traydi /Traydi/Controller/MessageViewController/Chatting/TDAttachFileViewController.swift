//
//  TDAttachFileViewController.swift
//  Traydi
//
//  Created by mac on 09/01/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit

class TDAttachFileViewController: TDBaseViewController {
    
    var arrAttachFile = [AttachFileModel]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrAttachFile = [AttachFileModel(imageName:"Camera", fileName:"camera"),
                         AttachFileModel(imageName:"Gallrey", fileName:"gallery2"),
                         AttachFileModel(imageName:"Video", fileName:"video"),
                         AttachFileModel(imageName:"Document", fileName:"document4"),
                         AttachFileModel(imageName:"Audio", fileName:"audio5"),
                         AttachFileModel(imageName:"Location", fileName:"location6"),
        ]

        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionSwipeUP(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension TDAttachFileViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrAttachFile.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TDAttachFileCollectionViewCell", for: indexPath) as! TDAttachFileCollectionViewCell
        let dict = arrAttachFile[indexPath.row]
        cell.imgAttach.image = UIImage(named:dict.fileName!)
        cell.imgAttach.tintColor = UIColor.orange
        cell.lblAttachFileName.text = dict.imageName
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.size.width / 2) - 50
        let height = (collectionView.frame.size.height / 3) - 50
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    }
    
}

class AttachFileModel: NSObject {
    var imageName:String?
    var fileName:String?
    
    init(imageName:String, fileName:String) {
        self.imageName = imageName
        self.fileName = fileName
    }
}
