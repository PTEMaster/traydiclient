//
//  TDConnectionViewController.swift
//  Traydi
//
//  Created by mac on 18/01/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit

class TDConnectionViewController: TDBaseViewController {
    @IBOutlet weak var viewTop: UIView!
 
    @IBOutlet weak var tableViewConnection: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.rightandLeftBottmCornerRadius(view: viewTop)
    }
    


}

extension TDConnectionViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IdentifierConnection", for: indexPath) as! TDConnectionTableViewCell
        return cell
    }
}
