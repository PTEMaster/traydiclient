//
//  TDConectionRequestAndSendViewController.swift
//  Traydi
//
//  Created by mac on 21/01/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit

class TDConnectionRequestAndSendViewController: TDBaseViewController {
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var btnRequest: UIButton!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var tableViewConnection: UITableView!
    @IBOutlet weak var lblRequestTotal: UILabel!
    @IBOutlet weak var lblRequestSendTotal: UILabel!
    
    var requestList = [RequestModal]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        btnRequest.isSelected = true
        btnSend.isSelected = false
         self.isCallRequestApiAndRequestSent(apiAction: ApiAction.TradersAndTradees.krequest_list)
        // Do any additional setup after loading the view.
    }

 @IBAction func actions(_ sender: UIButton) {
        switch sender.tag {
        case 2001: // action for Requests
            sender.isSelected = true
            btnSend.isSelected = false
        self.isCallRequestApiAndRequestSent(apiAction: ApiAction.TradersAndTradees.krequest_list)
        case 2002: // action for send
            sender.isSelected = true
            btnRequest.isSelected = false
            self.isCallRequestApiAndRequestSent(apiAction: ApiAction.TradersAndTradees.krequest_send_list)
        default:
            break
        }
        tableViewConnection.reloadData()
    }
    
    func isCallRequestApiAndRequestSent(apiAction:String) {
        var params = [String:Any]()
        let loginUser = AppSignleHelper.shard.login.userinfo
        params[ApiConstants.key.kuser_id] = "25"
        APIManager.share.delegate = self
        APIManager.share.performMaltipartPost(params: params, apiAction: apiAction)
    }
}

extension TDConnectionRequestAndSendViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return requestList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TDConnectionTableViewCell", for: indexPath) as! TDConnectionTableViewCell
        let dict = requestList[indexPath.row]
        cell.delegate = self
        cell.lblUserName.text = dict.full_name
        if self.btnSend.isSelected {
            cell.viewPending.isHidden = false
            cell.btnAccepts.isHidden = true
            cell.btnCancel.isHidden = true
            lblRequestSendTotal.text = "\(requestList.count)"
        } else {
            cell.viewPending.isHidden = true
            cell.btnAccepts.isHidden = false
            cell.btnCancel.isHidden = false
            lblRequestTotal.text = "\(requestList.count)"
        }
        return cell
    }
}



extension TDConnectionRequestAndSendViewController: RequestConnectionDelegate {
    func handlePendingRequest(cell: TDConnectionTableViewCell) {
       /* guard let indexPath = tableViewConnection.indexPath(for: cell)  else {
            return
        } */
    }
    
    func handleAcceptRequest(cell: TDConnectionTableViewCell) {
        guard let indexPath = tableViewConnection.indexPath(for: cell)  else {
            return
        }
        self.isCallDeleteRequestAndAcceptRequestApicall(apiAction: ApiAction.TradersAndTradees.kaccept_request, indexPath: indexPath)
    }
    
    func handleCancelRequest(cell: TDConnectionTableViewCell) {
        guard let indexPath = tableViewConnection.indexPath(for: cell)  else {
            return
        }
        self.isCallDeleteRequestAndAcceptRequestApicall(apiAction: ApiAction.TradersAndTradees.kdelete_request, indexPath: indexPath)
    }
    
    func isCallDeleteRequestAndAcceptRequestApicall(apiAction:String, indexPath:IndexPath) {
        let dict = requestList[indexPath.row]
        var params = [String:Any]()
        let loginData = AppSignleHelper.shard.login.userinfo
        params[ApiConstants.key.ConnectionRequestkeys.kreceiver_id] = loginData?.id
        params[ApiConstants.key.ConnectionRequestkeys.ksender_id] = dict.id
        APIManager.share.performPostApiHandler(params: params, apiAction: apiAction, successCompletionHandler: { (response) in
            let data = response.result.value as! [String:Any]
            if data["message"] as? String == "1" {
                self.requestList.remove(at: indexPath.row)
                self.tableViewConnection.deleteRows(at: [indexPath], with: .left)
            }
        }) { (error) in
            Util.showAlertWithCallback(AppName, message: error.localizedDescription, isWithCancel: false)
        }
    }
}


//MARK:- Response Api's
extension TDConnectionRequestAndSendViewController:ApiManagerDelegate {
    func successResponse(response: Any, check: String) {
        guard let data = response as? Data else {
            return
        }
        Loader.hideLoader()
        if check == ApiAction.TradersAndTradees.krequest_list {
            do {
                let resultObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                let requestModal = DataFetcherRequestModal(parameters:resultObject)
                if requestModal.code == ApiResponseStatus.kAPI_Result_Success {
                    self.requestList = requestModal.requestList ?? []
                    self.tableViewConnection.reloadData()
                  
                } else {
                    Util.showAlertWithCallback(AppName, message: requestModal.message, isWithCancel: false)
                }
                
            } catch {
                print("Unable to parse JSON response")
            }
            
        }
    }
    
    func errorResponse(error: Any, check: String) {
        Loader.hideLoader()
        guard let errormsg = error as? Error else {
            return
        }
        Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
        
    }
}
