//
//  TDChattingViewController.swift
//  Traydi
//
//  Created by mac on 09/01/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit
import UserNotifications
import SDWebImage

class TDChattingViewController: TDBaseViewController ,UIScrollViewDelegate {
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var tVChatting: UITableView!
    @IBOutlet weak var tvMsg: UITextView!
    @IBOutlet weak var tvConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var tableConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var btnEmoji: UIButton!
    @IBOutlet weak var btnAttachment: UIButton!
    @IBOutlet weak var viewmsgBottom: NSLayoutConstraint!
      var imagePicker: TDImagePicker!
    
    @IBOutlet var viewZoom: UIView!
    @IBOutlet weak var imgUPLOAD: UIImageView!
    @IBOutlet weak var scrlImage: UIScrollView!
    var dictSearch : charSearchModal? // jisse chat karna hai
    var arrChatMsg  = [chatModal]()
     let userInfo = AppSignleHelper.shard.login.userinfo
    var arrDocument = [[String:Any]]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  SocketIOManager.sharedInstance.connectSocket()
      //  SocketIOManager.sharedInstance.establishConnection()
        // Do any additional setup after loading the view.
         //self.viewmsgBottom.constant -= (216 + 45)
        tvMsg.textColor = .gray
        tvMsg.delegate = self
         getAllChat()
        self.rightandLeftBottmCornerRadius(view: viewTop)
        self.setupNotificationkeyboard()
        self.registerNib(tableview: tVChatting)
        receiveChatData()
         imagePicker = TDImagePicker(presentationController: self, delegate: self)
             scrlImage.delegate = self

        
        scrlImage.minimumZoomScale = 0.1
        scrlImage.maximumZoomScale = 4.0
        scrlImage.zoomScale = 1.0
        scrlImage.delegate = self
        scrlImage?.addSubview(imgUPLOAD)
       
    
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
            return imgUPLOAD
    }
    

    
    
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          self.navigationController?.setNavigationBarHidden(false, animated: false)
      }
    
    override func viewWillDisappear(_ animated: Bool) {
  //   SocketIOManager.sharedInstance.closeConnection()
     }
      
    
    func registerNib(tableview:UITableView) {
        tableview.register(UINib(nibName: "TDChattingSenderTextTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "TDChattingSenderTextTableViewCell")
        tableview.register(UINib(nibName: "TDChattingRecevierTextTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "TDChattingRecevierTextTableViewCell")
         tableview.register(UINib(nibName: "MyStoryReactionCell", bundle: Bundle.main), forCellReuseIdentifier: "MyStoryReactionCell")
         tableview.register(UINib(nibName: "UploadImageCell", bundle: Bundle.main), forCellReuseIdentifier: "UploadImageCell")
        //recevier
        tableview.register(UINib(nibName: "ReceiverUploadImageCell", bundle: Bundle.main), forCellReuseIdentifier: "ReceiverUploadImageCell")
        tableview.register(UINib(nibName: "ReceiverStoryReactionCell", bundle: Bundle.main), forCellReuseIdentifier: "ReceiverStoryReactionCell")
        
        
        
        
    }
    
    func setupNotificationkeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
         print("keyboardWillShow")
        scrollToBottom()
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 1.0,
        delay: 0.0,
        options: [],
        animations: {
            self.viewmsgBottom.constant -= (keyboardFrame.size.height )
            self.tVChatting.frame.size.height = self.tVChatting.frame.height - keyboardFrame.size.height
        },
        completion: nil)
        
       /* UIView.animate(withDuration: 0.0) {
            self.viewmsgBottom.constant -= (keyboardFrame.size.height )
            print(self.viewmsgBottom.constant)
            self.tVChatting.frame.size.height = self.tVChatting.frame.height - keyboardFrame.size.height
        }*/
    }

    @objc func keyboardWillHide(notification: NSNotification){
         print("keyboardWillHide")
        UIView.animate(withDuration: 0.0) {
            self.viewmsgBottom.constant = 0
            self.tVChatting.frame.size.height = 0
        }
    }

    @IBAction func actionCamera(_ sender: UIButton) {
         imagePicker.present(from: sender)
    }
    
    @IBAction func actionAttachment(_ sender: Any) {
        
    }
    
    @IBAction func actionEmoji(_ sender: Any) {
        
    }
    
    @IBAction func actionSendMessage(_ sender: Any) {
        if tvMsg.text == "" {
            return
        }
        var params = [String:Any]()
        let userInfo = AppSignleHelper.shard.login.userinfo
        params["id"] = ""
        params["sender"] = userInfo?.id
        params["receiver"] = dictSearch?.id
        params["message"] = tvMsg.text.encodeEmoji
        params["created_at"] = ""
        params["receiver_image"] = (dictSearch?.image)!
        params["receiver_name"] =  (dictSearch?.full_name)!
        params["sender_image"] =  userInfo!.image!
        params["sender_name"] = userInfo?.full_name
        params["type"] = ""
        
        print("Send message by me\(params)")
        //emit (send)
        SocketIOManager.sharedInstance.defaultSocket.emit("SenderById" , with: [params])
        var cc = chatModal()
        cc.sender = userInfo?.id // my id
        cc.message = tvMsg.text
        arrChatMsg.append(cc)
        let indexPath = IndexPath.init(row: arrChatMsg.count - 1, section: 0)
        tVChatting.insertRows(at: [indexPath], with: .bottom)
        tVChatting.scrollToRow(at: indexPath, at: .bottom, animated: false)
        tvMsg.text = ""
        tvConstraintHeight.constant = 37
         self.view.layoutIfNeeded()
    }
}

extension TDChattingViewController : UITableViewDelegate , UITableViewDataSource,UploadImageDelegate {
    func showImage(myIndex: Int) {
        self.viewZoom.frame = self.view.frame
        self.view.addSubview(viewZoom)
        
        let obj = arrChatMsg[myIndex]
        print(obj.attachment)
        imgUPLOAD.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
        imgUPLOAD.sd_setImage(with: URL(string: obj.attachment ?? ""), placeholderImage: UIImage(named: "images"))
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrChatMsg.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = arrChatMsg[indexPath.row]
        if  obj.sender == userInfo?.id{
            if obj.type == "1" || obj.type == ""  {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TDChattingSenderTextTableViewCell", for: indexPath) as! TDChattingSenderTextTableViewCell
                cell.lblMsg.text = obj.message?.decodeEmoji
                return cell
            }else  if obj.type == "2"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "UploadImageCell", for: indexPath) as! UploadImageCell
                cell.imgUploaded.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
                cell.imgUploaded.sd_setImage(with: URL(string: obj.attachment ?? ""), placeholderImage: UIImage(named: "images"))
                cell.delegate = self
                cell.myIndex = indexPath.row
                return cell
                
            }
            else  {
                
                let cell1 = tableView.dequeueReusableCell(withIdentifier: "MyStoryReactionCell", for: indexPath) as! MyStoryReactionCell
                 cell1.lblNameToreact.text = obj.receiver_name ?? ""  + " story"
                if obj.message != ""{
                    cell1.viewImg.isHidden = true
                    cell1.lblMessage.text = obj.message
                    
                }else{
                    cell1.lblMessage.text = ""
                     cell1.viewImg.isHidden = false
                    cell1.imgReaction.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
                    cell1.imgReaction.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + (obj.image)!), placeholderImage: UIImage(named: "images"))
                }
                return cell1
            }
            
        }else{
            //
            if obj.type == "1" ||  obj.type == ""{
                let cell = tableView.dequeueReusableCell(withIdentifier: "TDChattingRecevierTextTableViewCell", for: indexPath) as! TDChattingRecevierTextTableViewCell
                cell.lblMsgReciver.text = obj.message?.decodeEmoji
                cell.imgReciver.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
                cell.imgReciver.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + (obj.sender_image)!), placeholderImage: UIImage(named: "images"))
                return cell
                
            }else if obj.type == "2" { //
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverUploadImageCell", for: indexPath) as! ReceiverUploadImageCell
                cell.imgUploaded.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
                cell.imgUploaded.sd_setImage(with: URL(string: obj.attachment ?? ""), placeholderImage: UIImage(named: "images"))
                 cell.imgUser.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
                cell.imgUser.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + (obj.sender_image)!), placeholderImage: UIImage(named: "images"))
                
                return cell
                
            }
            else  {
                //reaction
                let cell1 = tableView.dequeueReusableCell(withIdentifier: "ReceiverStoryReactionCell", for: indexPath) as! ReceiverStoryReactionCell
                 cell1.lblNameToreact.text = obj.receiver_name ?? ""  + " story"
                if obj.message != ""{
                    cell1.viewImg.isHidden = true
                    cell1.lblMessage.text = obj.message
                    
                }else{
                    cell1.lblMessage.text = ""
                     cell1.viewImg.isHidden = false
                   cell1.imgReaction.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
                    cell1.imgReaction.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + (obj.image)!), placeholderImage: UIImage(named: "images"))
                }
                cell1.imgUser.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
                 cell1.imgUser.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + (obj.sender_image)!), placeholderImage: UIImage(named: "images"))
                return cell1
            }
            
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 44)
        let lableView = UILabel()
        let lblx = (headerView.frame.size.width / 2)
       
        lableView.frame = CGRect(x: lblx - 50 , y: 5, width: 100, height: headerView.frame.height - 10)
        lableView.backgroundColor = .white
      //  lableView.text = "Today"
        lableView.textAlignment = .center
        lableView.font = UIFont.boldSystemFont(ofSize: 12)
        lableView.layer.cornerRadius = 17
        lableView.clipsToBounds = true
        headerView.addSubview(lableView)
      
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
     
}

extension TDChattingViewController:UITextViewDelegate ,TDImagePickerDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if tvMsg.textColor ==  .gray {
            tvMsg.textColor = .black
            tvMsg.text = ""
            btnEmoji.isHidden = true
            btnAttachment.isHidden = true
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if tvMsg.text.isEmpty {
            tvMsg.textColor = .gray
            tvMsg.text = "Write your message..."
            btnEmoji.isHidden = false
            btnAttachment.isHidden = false
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let textHeight = textView.contentSize.height
        if textHeight < 100 {
            tvConstraintHeight.constant = textHeight
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func didSelect(image: UIImage?) {
        if image == nil{
            return
        }
        self.arrDocument.removeAll()
         APIManager.share.delegate = self
        self.arrDocument.append([ApiConstants.key.kimagee:image as Any])
        let params = [String:Any]()
        print(arrDocument)
        let apiAction = ApiAction.TradersAndTradees.ksend_image
        APIManager.share.performImageUpload(params: params, apiAction: apiAction, imageParams: arrDocument)
    }

}
extension TDChattingViewController : ApiManagerDelegate {
   
    
    func getAllChat(){
        APIManager.share.delegate = self
         var params = [String:Any]()
        
        params[ApiConstants.key.chatMessage.ksender_id] = AppSignleHelper.shard.login.userinfo?.id
        params[ApiConstants.key.chatMessage.kreceiver_id] = dictSearch?.id
        let apiAction = ApiAction.TradersAndTradees.kmessage_list //
         APIManager.share.performMaltipartPost(params: params , apiAction: apiAction)
    }
    
    func receiveChatData(){
        let id  =  AppSignleHelper.shard.login.userinfo?.id // dictSearch?.id ?? ""
        print("-\(id!)-")
        print("ReceiverById\(id!)")
        // on recive
        SocketIOManager.sharedInstance.defaultSocket.on("ReceiverById\(id!)") { (receiveData, ask) in
            print("receiveData-\(receiveData)-")
            if receiveData.count > 0 {
                if   let dict =  receiveData[0] as? [String:Any]{
                    var cc = chatModal()
                   
                    cc.sender = dict["sender"] as? String ?? ""
                    cc.receiver = dict["receiver"] as? String ?? ""
                    cc.message = dict["message"] as? String ?? ""
                    cc.sender_image = dict["sender_image"] as? String ?? ""
                     cc.receiver_image = dict["receiver_image"] as? String ?? ""
                     cc.receiver_name = dict["receiver_name"] as? String ?? ""
                     cc.sender_name = dict["sender_name"] as? String ?? ""
                     cc.attachment = dict["attachment"] as? String ?? ""
                     cc.image = dict["image"] as? String ?? ""
                    self.arrChatMsg.append(cc)
                    
                    let indexPath = IndexPath.init(row: self.arrChatMsg.count - 1, section: 0)
                    self.tVChatting.insertRows(at: [indexPath], with: .bottom)
                    self.tVChatting.scrollToRow(at: indexPath, at: .bottom, animated: true)
                }
            }
        }
    }
    
    func successResponse(response: Any, check: String) {
        Loader.hideLoader()
        guard let data = response as? Data else {
            return
        }
        if check == ApiAction.TradersAndTradees.kmessage_list {
            
            guard let dataFetcher = try? JSONDecoder().decode(chat.self, from:data) else { return }
            if dataFetcher.code == ApiResponseStatus.kAPI_Result_Success {
                if dataFetcher.users!.count > 0 {
                     arrChatMsg = dataFetcher.users!
                    tVChatting.reloadData()
                    scrollToBottom()
                }
            }
        }else  if check == ApiAction.TradersAndTradees.ksend_image {
           do {
                           let resultObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                           let code  = resultObject["code"] as? String
                           let message = resultObject["message"] as? String
                           let  image = resultObject["image"] as? String
                           if code == ApiResponseStatus.kAPI_Result_Success {
                            var params = [String:Any]()
                            let userInfo = AppSignleHelper.shard.login.userinfo
                            params["id"] = ""
                            params["sender"] = userInfo?.id
                            params["receiver"] = dictSearch?.id
                            params["message"] = ""
                            params["created_at"] = ""
                            params["receiver_image"] = (dictSearch?.image)!
                            params["receiver_name"] =  (dictSearch?.full_name)!
                            params["sender_image"] =  userInfo!.image!
                            params["sender_name"] = userInfo?.full_name
                            params["type"] = "2"
                            params["image"] = image
                            
                            print("Send message by me\(params)")
                            //emit (send)
                            SocketIOManager.sharedInstance.defaultSocket.emit("SenderById" , with: [params])
                            var cc = chatModal()
                            cc.sender = userInfo?.id // my id
                            cc.image = image
                            cc.type = "2"
                            arrChatMsg.append(cc)
                            let indexPath = IndexPath.init(row: arrChatMsg.count - 1, section: 0)
                            tVChatting.insertRows(at: [indexPath], with: .bottom)
                            tVChatting.scrollToRow(at: indexPath, at: .bottom, animated: false)
                           // tvMsg.text = ""
                           // tvConstraintHeight.constant = 37
                             self.view.layoutIfNeeded()
                           } else {
                               Util.showAlertWithCallback(AppName, message: message, isWithCancel: false)
                           }
                           
                       } catch {
                           print("Unable to parse JSON response")
                       }
        }
        
    }
    func scrollToBottom(){
        DispatchQueue.main.async {
            if self.arrChatMsg.count > 0{
            let indexPath = IndexPath(row: self.arrChatMsg.count-1, section: 0)
            self.tVChatting.scrollToRow(at: indexPath, at: .bottom, animated: false)
        }
        }
    }
       
       func errorResponse(error: Any, check: String) {
           Loader.hideLoader()
           guard let errormsg = error as? Error else {
               return
           }
           Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
       }
}


extension String {
    var decodeEmoji: String{
        let data = self.data(using: String.Encoding.utf8);
        let decodedStr = NSString(data: data!, encoding: String.Encoding.nonLossyASCII.rawValue)
        if let str = decodedStr{
            return str as String
        }
        return self
    }
    var encodeEmoji: String{
        if let encodeStr = NSString(cString: self.cString(using: .nonLossyASCII)!, encoding: String.Encoding.utf8.rawValue){
            return encodeStr as String
        }
        return self
    }
}


