//
//  TDChattingRecevierTextTableViewCell.swift
//  Traydi
//
//  Created by mac on 16/01/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit

class ReceiverStoryReactionCell: UITableViewCell {

    @IBOutlet weak var imgReaction: TYImageView!
    @IBOutlet weak var lblNameToreact: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var viewImg: UIView!
    @IBOutlet weak var imgUser: TYImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
