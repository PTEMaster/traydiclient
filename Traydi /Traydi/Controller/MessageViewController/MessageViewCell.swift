//
//  MessageViewCell.swift
//  Traydi
//
//  Created by mac on 21/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit
class MessageViewCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    override func layoutSubviews() {
        self.bgView.layer.cornerRadius = 10.0
        self.imgView.layer.cornerRadius = self.imgView.frame.size.height / 2
    }
}
