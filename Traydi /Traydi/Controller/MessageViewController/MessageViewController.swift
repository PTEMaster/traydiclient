//
//  MessageViewController.swift
//  Traydi
//
//  Created by mac on 21/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit
import SDWebImage
class MessageViewController: UIViewController {
    
    @IBOutlet weak var viewHeaderHeightContant: NSLayoutConstraint!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btnMyConnection: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewSearch: UIView!
     @IBOutlet weak var txtSearch: UITextField!
   var arrSearch = [charSearchModal]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      SocketIOManager.sharedInstance.connectSocket()
             SocketIOManager.sharedInstance.establishConnection()
        //self.viewHeader.backgroundColor = UIColor(displayP3Red: 170.0/255.0, green: 54.0/255.0, blue: 12.0/255.0, alpha: 1.0)
        
        self.viewHeader.layer.cornerRadius = 30.0
        self.viewHeader.clipsToBounds = true
        self.viewHeader.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        self.btnMyConnection.layer.cornerRadius = 25.0
        self.tblView.layer.cornerRadius = 15.0
        let custombar = self.tabBarController as! CustomTabBarController
           custombar.isHiddenTabBar(hidden: true)
        viewSearch.isHidden = true
        viewHeaderHeightContant.constant = 50
        txtSearch.delegate = self
           txtSearch.addTarget(self, action: #selector(SearchDidChange(_:)), for: .editingChanged)
        getAllNameofChatPerson()
    }
  /*  override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SocketIOManager.sharedInstance.connectSocket()
        SocketIOManager.sharedInstance.establishConnection()
    }
    override func viewDidAppear(_ animated: Bool) {
        SocketIOManager.sharedInstance.connectSocket()
        SocketIOManager.sharedInstance.establishConnection()
    }*/
    
    override func viewWillDisappear(_ animated: Bool) {
       // SocketIOManager.sharedInstance.closeConnection()
    }
    func getAllNameofChatPerson(){
        APIManager.share.delegate = self
         var params = [String:Any]()
       //  params[ApiConstants.key.ktrader_id] =  AppSignleHelper.shard.login.userinfo?.id
        if AppSharedData.shared.isSeeker == true{
            params[ApiConstants.key.ktradee_id] =  AppSignleHelper.shard.login.userinfo?.id
         }else{
            params[ApiConstants.key.ktrader_id] =  AppSignleHelper.shard.login.userinfo?.id
        }
         if UserDefaults.standard.bool(forKey: "isSeeker") {
             params[ApiConstants.key.credentialsKey.user_type] = "tradees"
                } else {
         params[ApiConstants.key.credentialsKey.user_type] = "traders"
                }
         params[ApiConstants.key.chatMessage.ksearch] = ""
        let apiAction = ApiAction.TradersAndTradees.ksearch_users
         
         APIManager.share.performMaltipartPost(params: params , apiAction: apiAction)
    }
    @IBAction func backArrowBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchBtnAction(_ sender: Any) {
        viewSearch.isHidden = false
        viewHeaderHeightContant.constant = 100
    }
    
    @IBAction func cancelSearchAction(_ sender: Any) {
        viewSearch.isHidden = true
        viewHeaderHeightContant.constant = 50
    }
    
    @IBAction func myConnectionRequestBtnAction(_ sender: Any) {
    
    }
}

extension MessageViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrSearch.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tblView.dequeueReusableCell(withIdentifier: "MessageViewCell", for: indexPath) as! MessageViewCell
         let obj = arrSearch[indexPath.row]
        cell.lblTitle.text = obj.full_name
        cell.imgView.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
        cell.imgView.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + obj.image! ), placeholderImage: UIImage(named: "imguser"))
      
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        print("tableview did select row:\(indexPath.row)")
        let obj = arrSearch[indexPath.row]
        let vc = TDChattingViewController.instance(.chatting) as! TDChattingViewController
        vc.dictSearch = obj  //chat with person
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


// MARK: - Textfield delegate
extension MessageViewController: UITextFieldDelegate, ApiManagerDelegate {
    func successResponse(response: Any, check: String) {
        Loader.hideLoader()
        guard let data = response as? Data else {
            return
        }
        if check == ApiAction.TradersAndTradees.ksearch_users {
            guard let dataFetcher = try? JSONDecoder().decode(charSearch.self, from:data) else { return }
            arrSearch.removeAll()
                       if dataFetcher.code == ApiResponseStatus.kAPI_Result_Success {
                          arrSearch = dataFetcher.users!
                        tblView.reloadData()
                       
            }
            
            
        }
    }
    
    func errorResponse(error: Any, check: String) {
        Loader.hideLoader()
        guard let errormsg = error as? Error else {
            return
        }
        Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
    }
    
    
    @objc func SearchDidChange(_ textField: UITextField) {
       let textToSearch = textField.text ?? ""
      //  if textToSearch != "" {
            APIManager.share.delegate = self
            var params = [String:Any]()
          //  params[ApiConstants.key.ktrader_id] =  AppSignleHelper.shard.login.userinfo?.id
        if AppSharedData.shared.isSeeker == true{
                  params[ApiConstants.key.ktradee_id] =  AppSignleHelper.shard.login.userinfo?.id
               }else{
                  params[ApiConstants.key.ktrader_id] =  AppSignleHelper.shard.login.userinfo?.id
              }
            if UserDefaults.standard.bool(forKey: "isSeeker") {
                params[ApiConstants.key.credentialsKey.user_type] = "tradees"
                   } else {
            params[ApiConstants.key.credentialsKey.user_type] = "tradees"
                   }
            params[ApiConstants.key.chatMessage.ksearch] = textToSearch
           let apiAction = ApiAction.TradersAndTradees.ksearch_users
            
            APIManager.share.performMaltipartPost(params: params , apiAction: apiAction)
      //  } performActionGetApi  performMaltipartPost
          
    }
}


struct charSearch : Codable {
    let code : String?
    let message : String?
    var users : [charSearchModal]?

    enum CodingKeys: String, CodingKey {

        case code = "code"
        case message = "message"
        case users = "users"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(String.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        users = try values.decodeIfPresent([charSearchModal].self, forKey: .users)
    }

}

struct charSearchModal : Codable {
    let id : String?
    let full_name : String?
    let image : String?
    let user_type : String?
    


    enum CodingKeys: String, CodingKey {

        case id = "id"
        case full_name = "full_name"
        case image = "image"
        case user_type = "user_type"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        full_name = try values.decodeIfPresent(String.self, forKey: .full_name)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        user_type = try values.decodeIfPresent(String.self, forKey: .user_type)
    }

}

