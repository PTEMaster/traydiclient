//
//  Guide02ViewController.swift
//  Traydi
//
//  Created by mac on 09/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//
import UIKit

class Guide02ViewController: UIViewController {
    
     @IBOutlet var scGallery: UIScrollView!
    @IBOutlet var pageController: UIPageControl!
     var totalX: CGFloat = 0
     var indexOfView = Int()
    var arrimg = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.navigationController?.isNavigationBarHidden = true
        
       
        if AppSharedData.shared.isSeeker == true{ // Seeker
             arrimg = [ "splash3", "splash4", "splash5" ]
        }else{ // Hire
             arrimg = [ "splash_3", "splash_4", "splash_5" ]
        }
       // Local images
        self.makeImgForSwipe()
        self.configurePageControl()
      
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pushNextSegueIdentifier" {
            //segue.destination
        }
    }
    @IBAction func nextBtnClicked(_ sender: Any) {
    let total  =  1 + pageController.currentPage
        if  arrimg.count - total != 0 {
            self.indexOfView += 1
                        pageController.currentPage = Int(indexOfView)
                        let offSetX = self.scGallery.contentOffset.x
                        let point = CGPoint(x: offSetX + scGallery.frame.size.width, y: 0)
                         print(point.x)
                        self.scGallery.setContentOffset(point, animated: true)
                        let deadlineTime = DispatchTime.now() + 0.5
                        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                            self.view.isUserInteractionEnabled = true
                        }
        }else{
            let sb = UIStoryboard(name: "Authentication", bundle: nil)
            let vc = sb.instantiateInitialViewController() // LoginViewController
            UIApplication.shared.windows[0].rootViewController = vc
            
            }
    }
   
    @IBAction func skipBtnAction(_ sender: Any) {
         AppSharedData.shared.isUserGuideSkipped = true
        
        let sb = UIStoryboard(name: "Authentication", bundle: nil)
        let vc = sb.instantiateInitialViewController() // LoginViewController
        UIApplication.shared.windows[0].rootViewController = vc
    }
    
    func makeImgForSwipe() {
         
         for i in 0..<arrimg.count {
             
             let imgView = UIImageView()
             
             imgView.frame = CGRect(x: totalX, y: 0, width: scGallery.frame.size.width, height: scGallery.frame.size.height)
             
             totalX += imgView.frame.size.width
             
             let imgurl = arrimg[i] as! String
             imgView.image = UIImage(named: imgurl)
            imgView.contentMode = .scaleToFill
             scGallery.addSubview(imgView)
             scGallery.autoresizesSubviews = true
         }
         scGallery.contentSize = CGSize(width: totalX, height: 0)
         scGallery.isScrollEnabled = false
         pageController.isHidden = false
         
         let rightGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRight))
         rightGestureRecognizer.direction = (.right)
         scGallery.addGestureRecognizer(rightGestureRecognizer)
         
         let leftGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeLeft))
         leftGestureRecognizer.direction = (.left)
         scGallery.addGestureRecognizer(leftGestureRecognizer)
     }
     
     
     
     /**
      Called when user swipes courosal view to right side.
      */
     
    @objc func swipeRight() {
         
         self.view.isUserInteractionEnabled = false
         if indexOfView == 0 {
             self.view.isUserInteractionEnabled = true
         } else {
             self.indexOfView -= 1
             pageController.currentPage = Int(indexOfView)
             
             let offSetX = self.scGallery.contentOffset.x
             let point = CGPoint(x: offSetX - scGallery.frame.size.width, y: 0)
             self.scGallery.setContentOffset(point, animated: true)
             let deadlineTime = DispatchTime.now() + 0.5
             DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                 self.view.isUserInteractionEnabled = true
             }
         }
     }
     // kundan
     /**
      Called when user swipes courosal view to left side.
      */
     
    @objc func swipeLeft() {
         self.view.isUserInteractionEnabled = false
         if indexOfView == arrimg.count - 1{
             self.view.isUserInteractionEnabled = true
         } else {
             self.indexOfView += 1
             pageController.currentPage = Int(indexOfView)
             let offSetX = self.scGallery.contentOffset.x
             let point = CGPoint(x: offSetX + scGallery.frame.size.width, y: 0)
              print(point.x)
             self.scGallery.setContentOffset(point, animated: true)
             let deadlineTime = DispatchTime.now() + 0.5
             DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                 self.view.isUserInteractionEnabled = true
             }
         }
     }
    
    func configurePageControl() {
           self.pageController.numberOfPages = self.arrimg.count
           self.pageController.currentPage = 0
           self.pageController.pageIndicatorTintColor = UIColor.lightGray
           self.pageController.currentPageIndicatorTintColor = appColor
           self.pageController.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
           self.pageController.frame.origin.y = (scGallery.frame.size.height + self.scGallery.frame.origin.y) - 20
           self.scGallery.addSubview(pageController)
       }
    
    
}
