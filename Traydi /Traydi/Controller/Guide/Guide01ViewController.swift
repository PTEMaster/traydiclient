//
//  Guide01ViewController.swift
//  Traydi
//
//  Created by mac on 09/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//   // hire false and seek truue

import UIKit

class Guide01ViewController: UIViewController {
    
    @IBOutlet weak var btnHire: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnSeek: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       // let storyboardGuide = UIStoryboard.init(name: T##String, bundle: T##Bundle?)
        //self.view.backgroundColor = UIColor.lightGray
        self.navigationController?.isNavigationBarHidden = true
        self.btnNext.layer.cornerRadius = 25.0
        self.isSelectedSeekHire(isStatus: (UserDefaults.standard.value(forKey: "isSeeker") as? Bool ?? false))
    }
    
    
    func isSelectedSeekHire(isStatus:Bool) {
  
        if isStatus{
            btnHire.isSelected = false
            btnSeek.isSelected = true
        }else{
            btnHire.isSelected = true
            btnSeek.isSelected = false
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pushNextSegueIdentifier" {
            //segue.destination
        }
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
       // self.performSegue(withIdentifier: "pushNextSegueIdentifier", sender: nil)
        let vc = Guide02ViewController.instance(.guide) as! Guide02ViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
   
  //
    //
    @IBAction func seekHireBtnClicked(_ sender: UIButton) {
        switch sender.tag {
        case 2001: // action hire Traders
             UserDefaults.standard.set(false, forKey: "isSeeker")
             self.isSelectedSeekHire(isStatus: false)
        case 2002: // action seek  Tradees
            UserDefaults.standard.set(true, forKey: "isSeeker")
             self.isSelectedSeekHire(isStatus: true)
        default:
            break
        }
        UserDefaults.standard.synchronize()
        
    }
    
}

extension UIImage {
func imageWithColor(tintColor: UIColor) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)

    let context = UIGraphicsGetCurrentContext()!
    context.translateBy(x: 0, y: self.size.height)
    context.scaleBy(x: 1.0, y: -1.0);
    context.setBlendMode(.normal)

    let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height) as CGRect
    context.clip(to: rect, mask: self.cgImage!)
    tintColor.setFill()
    context.fill(rect)

    let newImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()

    return newImage
}
}
