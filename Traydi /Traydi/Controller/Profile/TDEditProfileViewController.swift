//
//  TDEditProfileViewController.swift
//  Traydi
//
//  Created by mac on 07/02/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit
import  SDWebImage

class TDEditProfileViewController: TDBaseViewController {
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var textLastName: UITextField!
    @IBOutlet weak var txtMobileNumber: UITextField!
    @IBOutlet weak var txtVAddress: UITextView!
    @IBOutlet weak var imgVUserProfile: TYImageView!
    @IBOutlet weak var viewHeader: UIView!
    
    var imagepicker:TDImagePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagepicker = TDImagePicker(presentationController: self
        , delegate: self)
        self.rightandLeftBottmCornerRadius(view: viewHeader)
        // Do any additional setup after loading the view.
        txtVAddress.text = "Address..."
        txtVAddress.textColor = UIColor.lightGray
        txtVAddress.delegate = self
        self.navigationController?.navigationBar.tintColor = .white
        self.userDataSet()
    }
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           let custombar = self.tabBarController as! CustomTabBarController
           custombar.isHiddenTabBar(hidden: true)
           self.navigationController?.setNavigationBarHidden(false, animated: true)
       }
    
    @IBAction func actions(_ sender: UIButton) {
        switch sender.tag {
        case 2001, 2003:
       //     self.performSegue(withIdentifier: "identifierAddPhoto", sender: sender)
         //   imagepicker.present(from: sender)
            let acController = TDSettingsAddPhotoViewController.instance(.profile) as! TDSettingsAddPhotoViewController
            acController.delegate = self
            acController.modalPresentationStyle = .overCurrentContext
            acController.modalTransitionStyle = .crossDissolve
            self.present(acController, animated: true, completion: nil)
        case 2002:
          let valid = Validation()
            if valid.status {
            self.isSetupEditApiCall()
            }else {
                Util.showAlertWithCallback(AppName, message: valid.message, isWithCancel: false)
            }
        default:
            break
        }
    }
    func Validation() -> (message:String, status:Bool) {
           var message:String = ""
           var status:Bool = true
         if !self.isValidEmail(textLastName.text!) {
            message = UseCaseMessage.validation.empty.kemailValidation
            status = false
        }
         return (message:message, status: status)
    }
    override func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    //MARK:- Segue
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         
         if segue.identifier == "identifierAddPhoto" {
             let vc = segue.destination as! TDSettingsAddPhotoViewController
             vc.delegate = self
             vc.modalPresentationStyle = .overFullScreen
             //vc.modalTransitionStyle = .coverVertical
         }
        }
    
    func userDataSet(){
        let loginData = AppSignleHelper.shard.login.userinfo
        txtFirstName.text = loginData?.full_name
        txtMobileNumber.text = loginData?.mobile_number
        txtVAddress.text = loginData?.address
        textLastName.text = loginData?.email
        //textLastName.isUserInteractionEnabled = false
        txtVAddress.textColor = UIColor.black
        self.imgVUserProfile.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
         self.imgVUserProfile.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + loginData!.image!), placeholderImage: UIImage(named: "camerawhite"))
      //  imgVUserProfile.cacheImage(urlString: ImagesUrl.baseProfileUri + loginData!.image!)
    }
    
    func isSetupEditApiCall() {
        let apiAction = ApiAction.TradersAndTradees.kedit_profile
         let loginData = AppSignleHelper.shard.login.userinfo
        var params = [String:Any]()
        params[ApiConstants.key.kfull_name] = txtFirstName.text!
        params[ApiConstants.key.kEmail] = textLastName.text
        params[ApiConstants.key.kuser_id] = loginData?.id
        params[ApiConstants.key.kaddress] = txtVAddress.text
        var paramsImages = [[String:Any]]()
        paramsImages.append([ApiConstants.key.kprofileImage:imgVUserProfile.image as Any])
        APIManager.share.delegate = self
        APIManager.share.performImageUpload(params: params, apiAction: apiAction, imageParams: paramsImages)
        
    }
    
}

extension TDEditProfileViewController: TDImagePickerDelegate {
    func didSelect(image: UIImage?) {
        imgVUserProfile.image = image
    }
}

extension TDEditProfileViewController: UITextFieldDelegate, UITextViewDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtVAddress.textColor == UIColor.lightGray {
            txtVAddress.text = ""
            txtVAddress.textColor = UIColor.black
        }
    }
    
    func  textViewDidEndEditing(_ textView: UITextView) {
        if txtVAddress.text.isEmpty {
            txtVAddress.text = "Address..."
            txtVAddress.textColor = UIColor.lightGray
            
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

extension TDEditProfileViewController : ApiManagerDelegate {
    func successResponse(response: Any, check: String) {
        guard let data = response as? Data else {
            return
        }
        if check == ApiAction.TradersAndTradees.kedit_profile {
            do {
                
                let resultObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                
                if resultObject[ApiResponseStatus.kcode] as! String == ApiResponseStatus.kAPI_Result_Success {
                    guard let loginModal = try? JSONDecoder().decode(LoginModal.self, from:data) else { return  }
                    AppSignleHelper.shard.login = loginModal
                    UserDefaults.standard.setUserData(value: data)
                  self.navigationController?.popViewController(animated: true)
                } else {
                    Util.showAlertWithCallback(AppName, message: (resultObject[ApiResponseStatus.kMessage] as! String), isWithCancel: false)
                }
                let apiActionNotification = ApiAction.TradersAndTradees.kget_notification
                
                let loginUser = AppSignleHelper.shard.login.userinfo
                
                var params = [String:Any]()
                params[ApiConstants.key.kuser_id] = loginUser?.id
                
                APIManager.share.performMaltipartPost(params: params
                    , apiAction: apiActionNotification)
                
            } catch {
                print("Unable to parse JSON response")
            }
            
        }
    }
    
    func errorResponse(error: Any, check: String) {
        Loader.hideLoader()
        guard let errormsg = error as? Error else {
            return
        }
        Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
    }
    
    
}


extension TDEditProfileViewController: TDProfileAddPhotoDelegate{
    func selectedPhoto(image: UIImage) {
        imgVUserProfile.image = image
    }
    
    
}
