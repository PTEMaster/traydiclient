//
//  TDProfileProjectTableViewCell.swift
//  Traydi
//
//  Created by mac on 25/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit

protocol TDProfileProjectCellDelegate:NSObject {
    func actionProjectPhotoGallery(Cell:TDProfileProjectTableViewCell)
}

class TDProfileProjectTableViewCell: UITableViewCell {
    
    weak var delegate:TDProfileProjectCellDelegate?
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblProjectName: UILabel!
    @IBOutlet weak var lblProjectStatus: UILabel!
    @IBOutlet weak var lblProjectDescription: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func isSetData(projectData:DataFetcherProjectModal) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/mm/dd"
        let startDate = dateFormatter.date(from: projectData.project_start_time!)
        let endDate = dateFormatter.date(from: projectData.project_end_time!)
         dateFormatter.dateFormat = "dd MMM"
        let strStartDate = dateFormatter.string(from: startDate!)
        let strEndDate = dateFormatter.string(from: endDate!)
        
        self.lblStartDate.text = strStartDate
        self.lblEndDate.text = strEndDate
        self.lblProjectName.text = projectData.project_name
        self.lblProjectDescription.text = projectData.job_description
    }
    
    @IBAction func actionProjectPhotoGallery(_ sender: Any) {
        delegate?.actionProjectPhotoGallery(Cell: self)
    }  
}
