//
//  TDSettingsDocumentTypeViewController.swift
//  Traydi
//
//  Created by mac on 28/04/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit

protocol TDDocumentTypeDelegate:NSObject {
    func selectDocumentType(items:DataFetcherJobTitleModal , indexPath:IndexPath)
}

class TDSettingsDocumentTypeViewController: UIViewController {

    weak var delegate:TDDocumentTypeDelegate?
    
    var arrDocumentType = [DataFetcherJobTitleModal]()
    var indexPaths: IndexPath!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}


extension TDSettingsDocumentTypeViewController: UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDocumentType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "identifierDocumentType", for: indexPath)
        cell.textLabel?.text = arrDocumentType[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.selectDocumentType(items: arrDocumentType[indexPath.row], indexPath: indexPaths)
        self.dismiss(animated: true, completion: nil)
    }
}

