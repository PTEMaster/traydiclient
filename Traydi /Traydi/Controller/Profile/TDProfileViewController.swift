//
//  TDProfileViewController.swift
//  Traydi
//
//  Created by mac on 25/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit
import  SDWebImage

class TDProfileViewController: TDBaseViewController {
    @IBOutlet weak var viewProject: GradientView!
    @IBOutlet weak var lblProfileLine: UILabel!
    @IBOutlet weak var lblTextProject: UILabel!
    @IBOutlet weak var viewQualification:GradientView!
    @IBOutlet weak var lblQalificationLine:UILabel!
    @IBOutlet weak var lblTextQalification:UILabel!
    @IBOutlet weak var viewReviews:GradientView!
    @IBOutlet weak var lblReviewsLine:UILabel!
    @IBOutlet weak var lblTextReviews:UILabel!
    @IBOutlet weak var btnProject:UIButton!
    @IBOutlet weak var btnQalification:UIButton!
    @IBOutlet weak var btnReviews:UIButton!
    @IBOutlet weak var tableviewProfile: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    var profileCellType = ProfileTypeCell.project
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var viewhire: TYImageView!
    @IBOutlet weak var lblProjectCompleted: UILabel!
    @IBOutlet weak var imgProjectCompletedAndJobs: UIImageView!
    @IBOutlet weak var lblProjectAndJobCounts: UILabel!
    @IBOutlet weak var imgActiveAndReviews: UIImageView!
    @IBOutlet weak var lblActiveAndReview: UILabel!
    @IBOutlet weak var lblActiveAndReviewCounts: UILabel!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var viewRating: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUserProfile: TYImageView!
    @IBOutlet weak var lblFollowrs: UILabel!
    @IBOutlet weak var lblFollowing: UILabel!
    @IBOutlet weak var lblHired: UILabel!
    
    var projectList = [DataFetcherProjectModal]()
    var qulificationList:DataFecherQualificationDocumentModal?
    var profileDetail : Profile?
    let userInfo = AppSignleHelper.shard.login?.userinfo
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //seeker one who is seraching a job
        // hire one who is hiring for job/Project
        
        if AppSharedData.shared.isSeeker {
            viewhire.isHidden = true
            imgProjectCompletedAndJobs.image = UIImage(named: "hired")
            lblProjectCompleted.text = "jobs"
            lblProjectAndJobCounts.text = userInfo?.notifiy_on_job_complete
            
            imgActiveAndReviews.image = UIImage(named: "usermessage")
            lblActiveAndReview.text = "Reviews"
            lblActiveAndReviewCounts.text = "0"
            lblFollowrs.text = userInfo?.followers
            lblFollowing.text = userInfo?.following
            
           // btnCamera.isUserInteractionEnabled = false
           // btnProfile.isUserInteractionEnabled = false
        } else {
            viewhire.isHidden = true
            imgProjectCompletedAndJobs.image = UIImage(named: "projeectcom")
            lblProjectCompleted.text = "Project Completed"
            lblProjectAndJobCounts.text = userInfo?.notifiy_on_job_complete
            imgActiveAndReviews.image = UIImage(named: "imgProjectActive")
            lblActiveAndReview.text = "Project Active"
            lblActiveAndReviewCounts.text = "0"
           // btnCamera.isUserInteractionEnabled = true
           // btnProfile.isUserInteractionEnabled = true
            lblFollowrs.text = userInfo?.followers
            lblFollowing.text = userInfo?.following
        }
       
        self.setupRegisterNib()
    }
    
   
       

    //MARK:- Custom Methods
    
    func isSetUserData() {
        let userData = AppSignleHelper.shard.login.userinfo
        self.lblUserName.text = userData?.full_name
      //  self.imgUserProfile.cacheImage(urlString: ImagesUrl.baseProfileUri + userData!.image!)
        self.imgUserProfile.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
         self.imgUserProfile.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + userData!.image!), placeholderImage: UIImage(named: "user"))
    }
    
    func setupRegisterNib(){
        self.isSelectedProject()
        tableviewProfile.delegate = self
        tableviewProfile.dataSource = self
        tableviewProfile.register(UINib(nibName: "TDProfileProjectTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "TDProfileProjectTableViewCell")
        tableviewProfile.register(UINib(nibName: "TDProfileQualificationTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "TDProfileQualificationTableViewCell")
        tableviewProfile.register(UINib(nibName: "TDProfileReviewsTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "TDProfileReviewsTableViewCell")
       // tableHeight.constant = self.view.frame.height - 88 // for stuck scroll
        self.tableviewProfile.isScrollEnabled = false
        //no need to write following if checked in storyboard
        self.scrollView.bounces = false
        self.tableviewProfile.bounces = true
        self.isSetUserData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        projectList.removeAll()
         self.isSelectedProject()
        profileCellType = ProfileTypeCell.project
         self.isProfileDetailApiCall()
         self.isSetUserData()
       
        let custombar = self.tabBarController as! CustomTabBarController
        custombar.isHiddenTabBar(hidden: false)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    //MARK:- Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "identifierAddPhoto" {
            let vc = segue.destination as! TDSettingsAddPhotoViewController
            vc.delegate = self
            vc.modalPresentationStyle = .overFullScreen
            //vc.modalTransitionStyle = .coverVertical
        }
       }
    
    //MARK:- UIButton Action
    @IBAction func actionAddPhoto(_ sender: Any) {
        self.performSegue(withIdentifier: "identifierAddPhoto", sender: sender)
    }
    @IBAction func actionAll(_ sender: UIButton) {
        if sender == btnProject {
            self.isSelectedProject()
            profileCellType = .project
            tableHeight.constant =  CGFloat(projectList.count * 135)  + 45
        } else if sender == btnQalification {
            self.isSelectedQalification()
            if (qulificationList?.list!.count) == nil {
                 tableHeight.constant =  300//CGFloat((qulificationList?.list!.count)!  * 240)
            }else{
                 tableHeight.constant =  CGFloat((qulificationList?.list!.count)!  * 240) + 45
                }
           
             profileCellType = .qaulification  //240
        } else if sender == btnReviews {
            self.isSelectedReview()
             profileCellType = .reviews
        }
         tableviewProfile.reloadData()
    }
    
    func isSelectedProject() {
        viewProject.startColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        viewProject.endColor = UIColor(named: "colorDarkOrange")!
        lblProfileLine.backgroundColor = UIColor.orange
        lblTextProject.textColor = UIColor.orange
        
        viewQualification.startColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        viewQualification.endColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lblQalificationLine.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lblTextQalification.textColor = UIColor.lightGray
        
        viewReviews.startColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        viewReviews.endColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lblReviewsLine.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lblTextReviews.textColor = UIColor.lightGray

    }
    
    func isSelectedQalification() {
        viewQualification.startColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        viewQualification.endColor = UIColor(named: "colorDarkOrange")!
        lblQalificationLine.backgroundColor = UIColor.orange
        lblTextQalification.textColor = UIColor.orange
        
        viewProject.startColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        viewProject.endColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lblProfileLine.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lblTextProject.textColor = UIColor.lightGray
        
        viewReviews.startColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        viewReviews.endColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lblReviewsLine.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lblTextReviews.textColor = UIColor.lightGray
    }
    
    func isSelectedReview() {
        viewReviews.startColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        viewReviews.endColor = UIColor(named: "colorDarkOrange")!
        lblReviewsLine.backgroundColor = UIColor.orange
        lblTextReviews.textColor = UIColor.orange
        
        viewProject.startColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        viewProject.endColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lblProfileLine.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lblTextProject.textColor = UIColor.lightGray
        
        viewQualification.startColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        viewQualification.endColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lblQalificationLine.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lblTextQalification.textColor = UIColor.lightGray
               
    }
    
    func isSetupProjectListApiCall() {
        let apiAction = ApiAction.TradersAndTradees.kproject_list
        
        var params = [String:Any]()
        params[ApiConstants.key.kuser_id] = AppSignleHelper.shard.login.userinfo?.id
        APIManager.share.delegate = self
        APIManager.share.performMaltipartPost(params: params, apiAction: apiAction)
  
    }
    
    func isProfileDetailApiCall() {
        let apiAction = ApiAction.TradersAndTradees.kprofile_detail
        
        var params = [String:Any]()
        if AppSharedData.shared.isSeeker == true{
           params[ApiConstants.key.ktradee_id] =  AppSignleHelper.shard.login.userinfo?.id
        }else{
           params[ApiConstants.key.ktrader_id] =  AppSignleHelper.shard.login.userinfo?.id
       }
        APIManager.share.delegate = self
        APIManager.share.performMaltipartPost(params: params, apiAction: apiAction)
  
    }
}

extension TDProfileViewController: UITableViewDataSource, UITableViewDelegate , UIScrollViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch profileCellType {
        case .project:
            return projectList.count
        case .qaulification:
            return qulificationList?.list?.count ?? 0
        case  .reviews:
            return 10
  
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        switch profileCellType {
        case .project:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TDProfileProjectTableViewCell", for: indexPath) as! TDProfileProjectTableViewCell
            cell.delegate = self
            cell.isSetData(projectData:projectList[indexPath.row])
            return cell
        case .qaulification:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TDProfileQualificationTableViewCell", for: indexPath) as! TDProfileQualificationTableViewCell
            let qulification = qulificationList?.list![indexPath.row]
            cell.lblDocumentType.text = qulification?.document_type
            cell.lblDate.text = qulification?.created_at
            cell.imgDocument.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
            cell.imgDocument.sd_setImage(with: URL(string: ImagesUrl.documentUrl + (qulification?.document!)!), placeholderImage: UIImage(named: "images"))
            return cell
        case .reviews:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TDProfileReviewsTableViewCell", for: indexPath) as! TDProfileReviewsTableViewCell
            return cell
        }
        
       // return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    
    
   func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.scrollView {
            tableviewProfile.isScrollEnabled = (self.scrollView.contentOffset.y >= 555)
        }
        if scrollView == self.tableviewProfile {
            self.tableviewProfile.isScrollEnabled = (tableviewProfile.contentOffset.y > 0)
        }
    }
}
extension TDProfileViewController:TDProfileProjectCellDelegate {
    func actionProjectPhotoGallery(Cell: TDProfileProjectTableViewCell) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "TDProjectPhotosViewController") as! TDProjectPhotosViewController
        if #available(iOS 13.0, *) {
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            self.navigationController?.pushViewController(vc, animated: true)
        }
        let custombar = self.tabBarController as! CustomTabBarController
        custombar.isHiddenTabBar(hidden: true)
    }
}


extension TDProfileViewController: ApiManagerDelegate {
    func successResponse(response: Any, check: String) {
        guard let data = response as? Data else {
            return
        }
        
        if check == ApiAction.TradersAndTradees.kproject_list {
            do {
                
                let resultObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                
                if resultObject[ApiResponseStatus.kcode] as! String == ApiResponseStatus.kAPI_Result_Success {
                    let projectlist = resultObject["projectlist"] as! [[String:Any]]
                    
                    for project in projectlist {
                        let projectModal = DataFetcherProjectModal(parameters: project)
                        self.projectList.append(projectModal)
                    }
                    
                    tableHeight.constant =  CGFloat(projectList.count * 135) + 45
                    self.tableviewProfile.reloadData()
                   
                } else {
                    Util.showAlertWithCallback(AppName, message: (resultObject[ApiResponseStatus.kMessage] as! String), isWithCancel: false)
                }
                let apiActionNotification = ApiAction.TradersAndTradees.kqualification_document_list
                let loginUser = AppSignleHelper.shard.login.userinfo
                
                var params = [String:Any]()
                params[ApiConstants.key.kuser_id] = loginUser?.id
                
                APIManager.share.performMaltipartPost(params: params
                    , apiAction: apiActionNotification)
                
            } catch {
                print("Unable to parse JSON response")
            }
            
        } else if check  == ApiAction.TradersAndTradees.kqualification_document_list {
            do {
                
                let resultObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                let qulification = DataFecherQualificationDocumentModal(parameters: resultObject)
                
                if qulification.code! {
                    qulificationList = qulification
                    self.tableviewProfile.reloadData()
                } else {
                    
                    Util.showAlertWithCallback(AppName, message: (resultObject[ApiResponseStatus.kMessage] as! String), isWithCancel: false)
                }
                
            } catch {
                print("Unable to parse JSON response")
            }
            
        }else  if check  == ApiAction.TradersAndTradees.kprofile_detail {
            guard var workType = try? JSONDecoder().decode(ProfileDetailModel.self, from:data) else { return  }
           
            
            isSetupProjectListApiCall()
          
            if workType.code == ApiResponseStatus.kAPI_Result_Success {
              
                profileDetail = workType.profile
                setProfileDetail()
                 
            } else {
                Util.showAlertWithCallback(AppName, message: workType.message, isWithCancel: false)
            }
           
        }
        
        
    }
    func setProfileDetail(){
        lblFollowrs.text = profileDetail?.followers
        lblFollowing.text = profileDetail?.followings
        lblProjectAndJobCounts.text = profileDetail?.completed_project ?? "0"
        lblActiveAndReviewCounts.text = profileDetail?.active_project ?? "0"
    }
    
    
    func errorResponse(error: Any, check: String) {
        Loader.hideLoader()
        guard let errormsg = error as? Error else {
            return
        }
        Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
    }
    
    
}

extension TDProfileViewController: TDProfileAddPhotoDelegate{
    func selectedPhoto(image: UIImage) {
        imgUserProfile.image = image
    }
    
    
}

enum ProfileTypeCell {
    case project
    case qaulification
    case reviews
}
