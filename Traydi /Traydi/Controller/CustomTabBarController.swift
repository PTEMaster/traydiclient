//
//  CustomTabBarController.swift
//  Traydi
//
//  Created by mac on 23/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit
import YPImagePicker
import AVFoundation
import AVKit
import Photos


class CustomTabBarController: UITabBarController {
    @IBOutlet var viewBottomTabbar: UIView!
    @IBOutlet weak var tabbarBtn1: UIButton!
    @IBOutlet weak var tabbarBtn2: UIButton!
    @IBOutlet weak var tabbarBtn3: UIButton!
    @IBOutlet weak var tabbarBtn4: UIButton!
    @IBOutlet weak var tabbarBtn5: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    var imagePicker = UIImagePickerController()
    var selectedItems = [YPMediaItem]()

    let selectedImageV = UIImageView()
    let pickButton = UIButton()
    let resultsButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let Y = self.view.frame.height - self.viewBottomTabbar.frame.height
        let frm = CGRect(x: 0, y: Y, width: self.view.frame.size.width, height: self.viewBottomTabbar.frame.height)
        self.viewBottomTabbar.frame = frm
        self.view.addSubview(self.viewBottomTabbar)
        self.view.bringSubviewToFront(self.viewBottomTabbar)
        self.tabBar.isHidden = true
        self.tabbarBtn1.isSelected = true
        if AppSharedData.shared.isSeeker{
            tabbarBtn2.setImage(UIImage(named: "jobs"), for: .normal)
            tabbarBtn2.setImage(UIImage(named: "jobscolor"), for: .selected)
        }else{
            tabbarBtn2.setImage(UIImage(named: "folder1"), for: .normal)
            tabbarBtn2.setImage(UIImage(named: "folder"), for: .selected)
        }
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(normalTap))
        tapGesture.numberOfTapsRequired = 1
        tabbarBtn3.addGestureRecognizer(tapGesture)
       
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longTap))
               longGesture.minimumPressDuration = TimeInterval(exactly: 0.5)!
               tabbarBtn3.addGestureRecognizer(longGesture)
    }
    
    @objc func normalTap(_ sender: UIGestureRecognizer){
           print("Normal tap")
        self.selectedIndex = 2
       }

       @objc func longTap(_ sender: UIGestureRecognizer){
           print("Long tap")
           if sender.state == .ended {
               print("UIGestureRecognizerStateEnded")
               //Do Whatever You want on End of Gesture
           }
           else if sender.state == .began {
               print("UIGestureRecognizerStateBegan.")
               self.showPicker()
           }
       }
       
    func selectTabIndex(index: Int){
           switch index {
           case 0:
               self.tabBtnAction(self.tabbarBtn1)
               break
           case 1:
                self.tabBtnAction(self.tabbarBtn2)
               break
           case 2:
                self.tabBtnAction(self.tabbarBtn3)
               break
           case 4:
                self.tabBtnAction(self.tabbarBtn4)
               break
           default:
               self.tabBtnAction(self.tabbarBtn5)
           }
       }
    
    @objc func actionlong () {
        
    }
    
    @IBAction func tabBtnAction(_ sender: UIButton) {
        self.tabbarBtn1.isSelected = false
        self.tabbarBtn2.isSelected = false
        self.tabbarBtn3.isSelected = false
        self.tabbarBtn4.isSelected = false
        self.tabbarBtn5.isSelected = false
        self.tabbarBtn1.tintColor = UIColor.gray
        self.tabbarBtn2.tintColor = UIColor.gray
        self.tabbarBtn3.tintColor = UIColor.gray
        self.tabbarBtn4.tintColor = UIColor.gray
        self.tabbarBtn5.tintColor = UIColor.gray
        
        if sender == tabbarBtn1 {
            self.tabbarBtn1.isSelected  = true
            self.tabbarBtn1.tintColor = UIColor(named: "colorBaseApp")
            self.selectedIndex = 0
        }else if sender == tabbarBtn2{
            self.tabbarBtn2.tintColor = UIColor(named: "colorBaseApp")
            self.tabbarBtn2.isSelected  = true
            self.selectedIndex = 1
        }else if sender == tabbarBtn3{
            self.tabbarBtn3.tintColor = UIColor(named: "colorBaseApp")
            self.tabbarBtn3.isSelected  = true
            self.selectedIndex = 2
        }else if sender == tabbarBtn4{
            self.tabbarBtn4.tintColor = UIColor(named: "colorBaseApp")
            self.tabbarBtn4.isSelected  = true
            self.selectedIndex = 3
        }else if sender == tabbarBtn5{
            self.tabbarBtn5.tintColor = UIColor(named: "colorBaseApp")
            self.tabbarBtn5.isSelected  = true
            self.selectedIndex = 4
        }
    }
    
    func isHiddenTabBar(hidden:Bool) {
         self.viewBottomTabbar.isHidden = hidden
        self.tabBarController?.tabBar.isHidden = hidden
    }
 func showResults() {
        if selectedItems.count > 0 {
            let gallery = YPSelectionsGalleryVC(items: selectedItems) { g, _ in
                g.dismiss(animated: true, completion: nil)
            }
            let navC = UINavigationController(rootViewController: gallery)
            self.present(navC, animated: true, completion: nil)
        } else {
            print("No items selected yet.")
        }
    }
    
    // MARK: - Configuration
   
    func showPicker() {
        
        var config = YPImagePickerConfiguration()

        config.library.mediaType = .photoAndVideo

        config.shouldSaveNewPicturesToAlbum = false
        config.video.compression = AVAssetExportPresetMediumQuality
        config.startOnScreen = .library
        config.screens = [.library, .photo, .video]
        config.video.libraryTimeLimit = 500.0
        config.showsCrop =  .none //.rectangle(ratio: (16/9))
        config.wordings.libraryTitle = "Gallery"
       //  config.colors.tintColor = .red // Right bar buttons (actions)
       // config.bottomMenuItemSelectedTextColour = .red
      //  config.bottomMenuItemUnSelectedTextColour = .green
        
        config.hidesStatusBar = false
        config.hidesBottomBar = false
        config.maxCameraZoomFactor = 2.0
        config.library.maxNumberOfItems = 5
        config.gallery.hidesRemoveButton = false
        config.library.preselectedItems = selectedItems
        
        let picker = YPImagePicker(configuration: config)
        picker.navigationBar.tintColor = UIColor.white
        picker.navigationBar.barTintColor = UIColor(named: "colorBaseApp")
        picker.navigationBar.isTranslucent = false
        picker.didFinishPicking { [unowned picker] items, cancelled in
            
            if cancelled {
                print("Picker was canceled")
                picker.dismiss(animated: true, completion: nil)
                return
            }
            _ = items.map { print("🧀 \($0)") }
            
            self.selectedItems = items
            if let firstItem = items.first {
                switch firstItem {
                case .photo(let photo):
                    
                    self.selectedImageV.image = photo.image
                    self.isSetupAddStoryApiCall(data: photo.image)
                case .video(let video):
                    self.selectedImageV.image = video.thumbnail
                    
                    let assetURL = video.url
                    let playerVC = AVPlayerViewController()
                    let player = AVPlayer(playerItem: AVPlayerItem(url:assetURL))
                    playerVC.player = player
                    
                    picker.dismiss(animated: true, completion: { [weak self] in
                        self?.present(playerVC, animated: true, completion: nil)
                        print("😀 \(String(describing: self?.resolutionForLocalVideo(url: assetURL)!))")
                    })
                }
            }
        }
        present(picker, animated: true, completion: nil)
    }
    
    func isSetupAddStoryApiCall(data:UIImage) {
        
        let apiAction = ApiAction.TradersAndTradees.kadd_story
        var params = [String:Any]()
        params[ApiConstants.key.kuser_id] = AppSignleHelper.shard.login.userinfo?.id
        params[ApiConstants.key.kuser_type] = AppSignleHelper.shard.login.userinfo?.user_type
        var paramsImage = [[String:Any]]()
        // FIXME: HIMANSHU PAL need to resize image
         let thumb1 = data.resized(withPercentage: 0.5)
        paramsImage.append([ApiConstants.key.kimages:thumb1!])
        APIManager.share.delegate = self
        APIManager.share.performImageUpload(params: params, apiAction: apiAction, imageParams: paramsImage)
 
    }
}

extension CustomTabBarController:ApiManagerDelegate {
    func successResponse(response: Any, check: String) {
        guard let data = response as? Data else {
            return
        }
        if  check == ApiAction.TradersAndTradees.kadd_story {
            do {
                let resultObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                let code  = resultObject["code"] as? String
                let message = resultObject["message"] as? String
                if code == ApiResponseStatus.kAPI_Result_Success {
                    Util.showAlertWithCallback(AppName, message: message, isWithCancel: false) {
                        self.dismiss(animated: true, completion: nil)
                    }
                    self.dismiss(animated: true, completion: nil)
                } else {
                    Util.showAlertWithCallback(AppName, message: message, isWithCancel: false)
                }
            } catch {
                print("Unable to parse JSON response")
            }
        }
    }
    
    func errorResponse(error: Any, check: String) {
        guard let errormsg = error as? Error else {
            return
        }
        Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
    }
    
    
}

// Support methods
extension CustomTabBarController {
    /* Gives a resolution for the video by URL */
    func resolutionForLocalVideo(url: URL) -> CGSize? {
        guard let track = AVURLAsset(url: url).tracks(withMediaType: AVMediaType.video).first else { return nil }
        let size = track.naturalSize.applying(track.preferredTransform)
        return CGSize(width: abs(size.width), height: abs(size.height))
    }
}
